Third-party software included with the installer:

- ffmpeg
Product: FFmpeg Win32 static build by Kyle Schwarz
Version: 20170620-ae6f6d4
URL: http://ffmpeg.zeranoe.com/builds/

- ImageMagick
Product: ImageMagick portable Win32 static at 16 bits-per-pixel
Version: 7.0.6
URL: http://www.imagemagick.org/

- pdftohtml
Product: pdftohtml Win32 binaries
Version: poppler 0.51
URL: http://blog.alivate.com.au/poppler-windows/
Poppler URL: http://poppler.freedesktop.org/

- gs
Product: Ghostscript Win32 binaries (GPL release)
Version: 9.21
URL: http://www.ghostscript.com/

- exiftool
Product: ExifTool by Phil Harvey
Version: 10.57
URL: http://www.sno.phy.queensu.ca/~phil/exiftool/

- java
Product: OpenJDK 8 binaries
Version: 1.8.0.131-1.b11
URL: https://github.com/ojdkbuild/ojdkbuild
