package eu.project.producer.initialization;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.client.NuxeoClient;
import org.nuxeo.client.objects.Document;
import org.nuxeo.client.objects.Documents;
import org.nuxeo.client.objects.acl.ACE;
import org.nuxeo.client.objects.user.Group;
import org.nuxeo.client.objects.user.User;
import org.nuxeo.client.objects.user.UserManager;
import org.nuxeo.client.objects.user.Users;
import org.nuxeo.client.spi.NuxeoClientRemoteException;


public class App 
{

	private static final Log log = LogFactory.getLog(App.class);
	
    public static void main( String[] args )
    {
    	String url = "http://localhost:8080/nuxeo";
		Document doc;
		Document document;

		NuxeoClient nuxeoClient = new NuxeoClient.Builder().url(url).authentication("Administrator", "Administrator")
				.connect();

		List<String[]> map = new ArrayList<String[]>();

		// checkpath, parentPath, Type, Name
		map.add(new String[] { "/Producer_Repository", "/", "Domain", "Producer_Repository" });
		map.add(new String[] { "/Producer_Repository/workspaces/Image", "/Producer_Repository/workspaces/",
				"Workspace", "Image" });
		map.add(new String[] { "/Producer_Repository/workspaces/Video", "/Producer_Repository/workspaces/",
				"Workspace", "Video" });
		map.add(new String[] { "/Producer_Repository/workspaces/Audio", "/Producer_Repository/workspaces/",
				"Workspace", "Audio" });
		map.add(new String[] { "/Producer_Repository/workspaces/Text", "/Producer_Repository/workspaces/",
				"Workspace", "Text" });
		map.add(new String[] { "/Producer_Repository/AATtraining", "/Producer_Repository/",
				"WorkspaceRoot", "AATtraining" });
		map.add(new String[] { "/Producer_Repository/AATtraining/categories", "/Producer_Repository/AATtraining/",
				"Workspace", "categories" });

		for (int i = 0; i < map.size(); i++) {

		
				// Fetch the root document
				//doc = nuxeoClient.repository().fetchDocumentByPath(map.get(i)[0]);

				Documents checkfolder = nuxeoClient.repository().query("SELECT * FROM Document WHERE ecm:path='" + map.get(i)[0] + "'");
				if(checkfolder.size() > 0) {
				
				log.info("The document on the PATH:" + map.get(i)[0] + " has already been created!");

				}
				else{
					// Create a document
					document = Document.createWithName(map.get(i)[0], map.get(i)[2]);

					document.setPropertyValue("dc:title", map.get(i)[3]);

					document = nuxeoClient.repository().createDocumentByPath(map.get(i)[1], document);
					log.info("The document on the PATH:" + map.get(i)[0] + " has been created!");
				}

		}

		// remove obsolete folder PATH, TYPE
		map.clear();
		map.add(new String[] { "/Producer_Repository/templates", "Workspace" });
		map.add(new String[] { "/Producer_Repository/sections", "Workspace" });

		for (int i = 0; i < map.size(); i++) {

			try {
				// Fetch the root document
				//doc = nuxeoClient.operation("Repository.GetDocument").param("value", map.get(i)[0]).execute();
				Documents checkfolder = nuxeoClient.repository().query("SELECT * FROM Document WHERE ecm:path='" + map.get(i)[0] + "'");
				if(checkfolder.size() > 0) {
					nuxeoClient.repository().deleteDocument(checkfolder.getDocument(0));
					log.info("The document on the PATH:" + map.get(i)[0] + " has been removed!");
				}else{
					log.info("The document on the PATH:" + map.get(i)[0] + " doesn't exist.The remove operation not needed.");
				}

			} catch (NuxeoClientRemoteException e) {
				log.info("The document on the PATH:" + map.get(i)[0] + " has already been removed!");
			}

		}

		UserManager userManager = nuxeoClient.userManager();
				
		List<String> newgroups = new ArrayList<String>();
		newgroups.add("PRODUCER");
		newgroups.add("INTEGRATION");

		
		for (int i = 0; i < newgroups.size(); i++) {
		
				Group group = new Group();
				String name = newgroups.get(i);

				
				try{
					group = userManager.fetchGroup(name);
					log.info("The group "+name+" has already created!");
				} catch (NuxeoClientRemoteException e) {
					group.setGroupName(name);
					group.setGroupLabel(name);
					group = userManager.createGroup(group);
					log.info("The group "+name+" has been created!");
				}
		}

		
		// add Permissions PATH, TYPE, User/Group
		map.clear();

		map.add(new String[] { "/Producer_Repository", "everything", "PRODUCER" });
		map.add(new String[] { "/Producer_Repository/workspaces", "everything", "PRODUCER" });
		map.add(new String[] { "/Producer_Repository/workspaces/Image", "everything", "PRODUCER" });
		map.add(new String[] { "/Producer_Repository/workspaces/Video", "everything", "PRODUCER" });
		map.add(new String[] { "/Producer_Repository/workspaces/Audio", "everything", "PRODUCER" });
		map.add(new String[] { "/Producer_Repository/workspaces/Text", "everything", "PRODUCER" });
		map.add(new String[] { "/Producer_Repository/AATtraining", "everything", "PRODUCER" });
		map.add(new String[] { "/Producer_Repository/AATtraining/categories", "everything", "PRODUCER" });
		

		map.add(new String[] { "/Producer_Repository", "everything", "INTEGRATION" });
		map.add(new String[] { "/Producer_Repository/workspaces", "everything", "INTEGRATION" });
		map.add(new String[] { "/Producer_Repository/workspaces/Image", "everything", "INTEGRATION" });
		map.add(new String[] { "/Producer_Repository/workspaces/Video", "everything", "INTEGRATION" });
		map.add(new String[] { "/Producer_Repository/workspaces/Audio", "everything", "INTEGRATION" });
		map.add(new String[] { "/Producer_Repository/workspaces/Text", "everything", "INTEGRATION" });
		map.add(new String[] { "/Producer_Repository/AATtraining", "everything", "INTEGRATION" });
		map.add(new String[] { "/Producer_Repository/AATtraining/categories", "everything", "INTEGRATION" });

		
		for (int i = 0; i < map.size(); i++) {
			
			Documents checkfolder = nuxeoClient.repository().query("SELECT * FROM Document WHERE ecm:path='" + map.get(i)[0] + "'");

			if(checkfolder.size() > 0)
			{			
					Document folder = nuxeoClient.repository().fetchDocumentByPath(map.get(i)[0]);
		
					ACE ace = new ACE();
					ace.setUsername(map.get(i)[2]);
					ace.setPermission(map.get(i)[1]);
					ace.setCreator("Administrator");
					ace.setBlockInheritance(false);
					folder.addPermission(ace);
			}else{
				log.info("The folder " + map.get(i)[0] + " doesn't exist!");
			}
		}
		
     /* delete user */
	/*	userManager.deleteUser(username);*/

		List<String> users = new ArrayList<>();
		users.add("OCD");
		users.add("IDT");
		users.add("ABT");
		users.add("IEVCT");
		users.add("SSF");
		users.add("AAT");
		users.add("SRPT");
		users.add("360VP");
		
		userManager = nuxeoClient.userManager();
		

		
		for (int i = 0; i < users.size(); i++) {
		
			try {
	
				User checkuser = userManager.fetchUser(users.get(i));
				userManager.addUserToGroup(users.get(i), "INTEGRATION");
				userManager.addUserToGroup(users.get(i), "administrators");
				
				log.info("The user " + checkuser.getUserName() + " has already been created and assigned to the groups INTEGRATION and administrators");
				
			} catch (NuxeoClientRemoteException e) {
				log.info("The "+users.get(i)+ " "+e.getMessage());

				User newUser = new User();
				newUser.setUserName(users.get(i));
				newUser.setCompany(users.get(i));
				newUser.setEmail((users.get(i))+"@producer-project.eu");
				newUser.setFirstName((users.get(i)));
				newUser.setLastName((users.get(i)));
				newUser.setPassword((users.get(i)));
				//newUser.setTenantId("mytenantid");
				List<String> groups = new ArrayList<String>();
				groups.add("INTEGRATION");
				groups.add("administrators");
				newUser.setGroups(groups);
				userManager.createUser(newUser);
				
				log.info("The user " + users.get(i) + " has been created!");
			}

		}

		
		nuxeoClient.disconnect();
    }



}
