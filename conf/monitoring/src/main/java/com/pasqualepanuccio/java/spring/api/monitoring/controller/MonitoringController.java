package com.pasqualepanuccio.java.spring.api.monitoring.controller;


import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.apache.commons.io.FileUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@RestController
public class MonitoringController {

    private static final Logger log = LogManager.getLogger(MonitoringController.class);
    

    @RequestMapping(value = "/echo")
    public ResponseEntity<String> echo(@RequestBody String body, @RequestHeader HttpHeaders headers) {
        log.info("Received Request");
        log.info("HEADERS");
        headers.entrySet().forEach(a -> {
            log.info(a.getKey() + ":" + a.getValue());
        });

        log.info("BODY");
        log.info(body);

        return ResponseEntity.ok(body);
    }
    
    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(value = "/post",method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> post(@RequestBody String body, @RequestHeader HttpHeaders headers) {
        log.info("Received Request");
        log.info("HEADERS");
        headers.entrySet().forEach(a -> {
            log.info(a.getKey() + ":" + a.getValue());
        });

        log.info("BODY");
        log.info(body);
        
        try {
    		
			//sleep 5 seconds
			Thread.sleep(5000);
			
			log.info("Wait 5 seconds.");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

        return ResponseEntity.ok(body);
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(value = "/annotate",method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> annotate(@RequestBody String body, @RequestHeader HttpHeaders headers) {
        log.info("Received Request");
        log.info("HEADERS");
        headers.entrySet().forEach(a -> {
            log.info(a.getKey() + ":" + a.getValue());
        });

        log.info("BODY");
        log.info(body);
        
        try {
    		
			//sleep 5 seconds
			Thread.sleep(5000);
			
			log.info("Wait 5 seconds.");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        String response = "";
        if(isJSONValid(body)) {
        	 response = "{ \"message\": \"The Annotation started!\"}";
        }else{
        	 response = "{ \"error\": \"Input JSON is not appropriate!\"}";
        }

       

        return ResponseEntity.ok(response);
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @RequestMapping(value = "/videos_to_target",method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> videos_to_target(@RequestBody String body, @RequestHeader HttpHeaders headers) throws JSONException {
        log.info("Received Request");
        log.info("HEADERS");
        headers.entrySet().forEach(a -> {
            log.info(a.getKey() + ":" + a.getValue());
        });

        log.info("BODY");
        log.info(body);
        
        try {
    		
			//sleep 5 seconds
			Thread.sleep(5000);
			
			log.info("Wait 5 seconds.");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
        }
        
        JSONObject requestbody = new JSONObject(body);

        JSONObject response = new JSONObject();

        JSONObject rapp1 = new JSONObject();
   
        JSONObject num_of_members = new JSONObject();

        num_of_members.accumulate("num_of_members","1");

        String listvideo = requestbody.get("videos").toString();
        
        String[] videolist = listvideo.split(",");
        
        List<String> list = new ArrayList<String>();
        
        list = Arrays.asList(videolist);

        JSONObject videoslist = new JSONObject();
        JSONArray videos = new JSONArray();
        
        Collections.reverse(list);

        for (int i = 0; i < list.size(); i++) {
            JSONObject video = new JSONObject();
            video.accumulate("video", list.get(i));
            video.accumulate("similarity", '0');
            videos.put(video);
        }

        rapp1.put("videos", videos);
        rapp1.put("num_of_members","1");

        response.accumulate("representative 1", rapp1);
        response.accumulate("representative 2", rapp1);
        
        log.info("reverse the videolist");

        
        

        return ResponseEntity.ok(response.toString());
    }
    
    
    @RequestMapping(value = "/mcssrlist" ,method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> mcssrlist(@RequestHeader HttpHeaders headers) {
        log.info("Received Request");
        log.info("HEADERS");
        headers.entrySet().forEach(a -> {
            log.info(a.getKey() + ":" + a.getValue());
        });

        try {
 
        	//Get file from resources folder
        	//ClassLoader classLoader = getClass().getClassLoader();
        	//File file = new File(classLoader.getResource("mcssr.json").getFile());
        	File file = new File("C:/Users/Giuseppe Filomena/projects/monitoring/src/main/resources/mcssr.json");
        	String content = FileUtils.readFileToString(file, "UTF-8");
 
            JSONObject jsonObject = new JSONObject(content);
            return ResponseEntity.ok(jsonObject.toString());

 
        } catch (Exception e) {
            e.printStackTrace();
        }

      
      
        return ResponseEntity.ok("Failed");
    
    }
    
    

 
     



      public static boolean isJSONValid(String jsonInString) {
    	  JSONObject o;
    	  try {
			o = new JSONObject(jsonInString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    	  
		return true;

        
      }
    
    
    
}
