package eu.project.producer.mcssr;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.nuxeo.common.Environment;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationContext;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.automation.core.util.BlobList;
import org.nuxeo.ecm.core.api.Blob;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.runtime.api.Framework;
import org.zeroturnaround.zip.ZipUtil;

import eu.project.producer.mcssr.exception.CreateDirectoryException;
import eu.project.producer.mcssr.exception.FileDeletionException;
import eu.project.producer.mcssr.exception.HttpClientException;

/**
 *
 */
@Operation(id = AATUploadingDB.ID, category = Constants.CAT_DOCUMENT, label = "AAT Uploading DB", description = "Service to Upload training faces using a Zip file towards the AAT.")
public class AATUploadingDB {

	public static final String ID = "Document.AATUploadingDB";

	private static final String aatModelEndpoint = Framework.getProperty("aat.model.service");

	private static final Log log = LogFactory.getLog(AATUploadingDB.class);

	private static final String aatCategoriesBaseDirectory = "/Producer_Repository/AATtraining/categories/";

	@Context
	protected CoreSession session;

	@Context
	protected AutomationService automationService;

	@Param(name = "uid", required = false)
	protected String uid;

	@Param(name = "path", required = false)
	protected String path;

	@OperationMethod
	public DocumentModel run() throws OperationException, IOException {
		String query = "SELECT * FROM Document where ecm:path STARTSWITH '" + aatCategoriesBaseDirectory + "'";
		DocumentModelList aatCategoriesList = (DocumentModelList) session.query(query);
		Path catDirectory = Paths.get(Environment.getDefault().getTemp().toString() + File.separator + "Categories");
		if (!Files.exists(catDirectory)) {
				try {
					Files.createDirectories(catDirectory);
				} catch (IOException e) {
					throw new CreateDirectoryException("Error during the creation of the Temporary directory");
				}
				for (DocumentModel dm : aatCategoriesList) {
					createCategoryStructure(dm, catDirectory);
				}
				createZip(catDirectory);

				File zipFile = Paths
						.get(Environment.getDefault().getTemp().toString() + File.separator + "Categories" + ".zip")
						.toFile();
				sendToAAT(zipFile);
				deleteFile(zipFile);
				deleteFile(catDirectory.toFile());
		}
		return null;
	}

	private void deleteFile(File file) throws FileDeletionException, IOException {
		try {
			if (Files.isDirectory(file.toPath())) {
				log.debug("Deleting the directory: " + file.toPath().toString());
				FileUtils.deleteDirectory(file);
				log.debug("Deleted the directory");
			} else {
				log.debug("Deleting the file: " + file.toPath().toString());
				Files.delete(file.toPath());
				log.debug("Deleted the file");
			}
		} catch (DirectoryNotEmptyException dnoe) {
			dnoe.printStackTrace();
			throw new FileDeletionException("Error during the delete of the file");
		} catch (SecurityException se) {
			se.printStackTrace();
			throw new FileDeletionException("Error during the delete of the file");
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void sendToAAT(File zipFile) throws OperationException, IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		log.debug("Send Model DB to AAT: " + aatModelEndpoint);
		// HttpPost httpPost = new HttpPost(aatHostname+":"+aatPort+"/model");
		HttpPost httpPost = new HttpPost(aatModelEndpoint);
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		builder.addBinaryBody("model", zipFile, ContentType.create("application/zip"), "Categories.zip");
		HttpEntity entity = builder.build();
		httpPost.setEntity(entity);

		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpPost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			throw new HttpClientException("Error during the execution of the service call");
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		if (response != null) {
			log.info("Response from AAT:" + response.getStatusLine());
			HttpEntity responseEntity = response.getEntity();
			log.debug("Response from AAT:");
			log.debug(responseEntity.toString());
		}
		try {
			httpclient.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void createZip(Path pp) {
		ZipUtil.pack(pp.toFile(), Paths
				.get(Environment.getDefault().getTemp().toString() + File.separator + "Categories" + ".zip").toFile());
	}

	private void createCategoryStructure(DocumentModel dm, Path catDirectory) throws OperationException, IOException {
		try {
			Path docRootPath = Paths.get(catDirectory.toString() + File.separator + dm.getName());
			Files.createDirectories(docRootPath);
			OperationContext ctx = new OperationContext(session);
			ctx.setInput(dm);
			BlobList blobs = null;
			try {
				blobs = (BlobList) automationService.run(ctx, "Document.GetBlobs");
			} catch (OperationException e) {
				String msg="Error during the retrieving of Blob list " + dm.getPropertyValue("dc:title") + "!";
				log.error(msg);
				throw e;
			}
			log.debug("Blob list size: " + blobs.size());
			for (Blob b : blobs) {
				FileUtils.copyInputStreamToFile(b.getStream(),
						Paths.get(docRootPath.toString() + File.separator + b.getFilename()).toFile());
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}

	}
}
