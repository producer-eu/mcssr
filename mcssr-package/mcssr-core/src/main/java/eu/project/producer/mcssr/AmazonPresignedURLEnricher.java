package eu.project.producer.mcssr;

import static org.nuxeo.ecm.core.io.registry.reflect.Instantiations.SINGLETON;
import static org.nuxeo.ecm.core.io.registry.reflect.Priorities.REFERENCE;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.model.Property;
import org.nuxeo.ecm.core.io.marshallers.json.enrichers.AbstractJsonEnricher;
import org.nuxeo.ecm.core.io.registry.reflect.Setup;
import org.nuxeo.runtime.api.Framework;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import eu.project.producer.mcssr.service.DocumentService;

@Setup(mode = SINGLETON, priority = REFERENCE)
public class AmazonPresignedURLEnricher extends AbstractJsonEnricher<DocumentModel> {

	// The enricher is called using enrichers.document: mcssrTags
	public static final String NAME = "playableURLs";

	private static final Log log = LogFactory.getLog(AmazonPresignedURLEnricher.class);

	private ObjectMapper mapper;

	@Context
	private DocumentService documentService=Framework.getService(DocumentService.class);

	public AmazonPresignedURLEnricher() {
		super(NAME);
	}

	@Override
	public void write(JsonGenerator jsonG, DocumentModel doc) throws IOException {
		mapper = new ObjectMapper();
		if (doc.getType().equals("Video")) {
			//jsonG.writeFieldName(NAME);
			jsonG.writeArrayFieldStart(NAME);
			List<VideoMapping> videoMappingList = getListOfVideoMapping(doc);
			for (VideoMapping m : videoMappingList) {
				writeVideoMappingJson(jsonG, m);
			}
			jsonG.writeEndArray();
		}
	}

	private void writeVideoMappingJson(JsonGenerator jsonG, VideoMapping vm) {
		try {
			jsonG.writeStartObject();
			jsonG.writeStringField("name", vm.getName());
			jsonG.writeStringField("digest", vm.getDigest());
			jsonG.writeStringField("url", vm.getUrl().toString());
			jsonG.writeStringField("mime-type", vm.getMimeType());
			jsonG.writeEndObject();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			log.error("Error during the serialization of Video Mapping Object to Json!"+e.getLocalizedMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("Error during the serialization of Video Mapping Object to Json!"+e.getLocalizedMessage());
		}
	}

	private List<VideoMapping> getListOfVideoMapping(DocumentModel doc) {
		List<VideoMapping> videoMappingList = new ArrayList<>();
		log.debug("Id of document whose blob is being retrieved: "+doc.getId());
		if (doc.getProperty("file:content") != null && doc.getProperty("file:content").get("digest") != null && doc.getProperty("file:content").get("digest").getValue()!=null) {
			String digest = (String) doc.getProperty("file:content").get("digest").getValue();
			URL url = documentService.getPreSignedAmazonURL(digest);
			String name = (String) doc.getProperty("file:content").get("name").getValue();
			String mimetype = (String) doc.getProperty("file:content").get("mime-type").getValue();
			videoMappingList.add(new VideoMapping(digest, name, url, mimetype));
		}
		if (doc.getProperty("vid:transcodedVideos") != null && doc.getProperty("vid:transcodedVideos").size() >= 1) {
			for (int i = 0; i < doc.getProperty("vid:transcodedVideos").size(); i++) {
				Property vidProperty = doc.getProperty("vid:transcodedVideos").get(i);
				String name = (String) vidProperty.get("content").get("name").getValue();
				String digest = (String) vidProperty.get("content").get("digest").getValue();
				String mimeType = (String) vidProperty.get("content").get("mime-type").getValue();
				URL url = documentService.getPreSignedAmazonURL(digest);
				videoMappingList.add(new VideoMapping(digest, name, url, mimeType));
			}
		}
		return videoMappingList;
	}

	@JsonSerialize(using = VideoMappingSerializer.class)
	class VideoMapping {
		private String digest;
		private String name;
		private URL url;
		private String mimeType;

		public VideoMapping(String digest, String name, URL url, String mimeType) {
			this.digest = digest;
			this.name = name;
			this.url = url;
			this.mimeType = mimeType;
		}

		protected String getDigest() {
			return digest;
		}

		protected void setDigest(String digest) {
			this.digest = digest;
		}

		protected String getName() {
			return name;
		}

		protected void setName(String name) {
			this.name = name;
		}

		protected URL getUrl() {
			return url;
		}

		protected void setUrl(URL url) {
			this.url = url;
		}

		protected String getMimeType() {
			return mimeType;
		}

		protected void setMimeType(String mimeType) {
			this.mimeType = mimeType;
		}

	}
	
	class VideoMappingSerializer extends StdSerializer<VideoMapping> {

	    public VideoMappingSerializer() {
	        this(null);
	    }

	    public VideoMappingSerializer(Class<VideoMapping> t) {
	        super(t);
	    }

		@Override
		public void serialize(VideoMapping value, com.fasterxml.jackson.core.JsonGenerator jgen, SerializerProvider arg2)
				throws IOException {
			jgen.writeStartObject();
	        jgen.writeStringField("name", value.getName());
	        jgen.writeStringField("digest", value.getDigest());
	        jgen.writeStringField("AmazonSignedUrl", value.getUrl().toString());
	        jgen.writeStringField("mime-type", value.getMimeType());
	        jgen.writeEndObject();
		}
	}

}
