package eu.project.producer.mcssr;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationContext;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.automation.core.util.Properties;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.ecm.core.api.IdRef;
import org.nuxeo.ecm.core.api.PathRef;

/**
 *
 */
@Operation(id = AnnotationDocumentService.ID, category = Constants.CAT_DOCUMENT, label = "AnnotationDocumentService", description = "Annotate document")
public class AnnotationDocumentService {

	public static final String ID = "Document.AnnotationDocumentService";

	private static final Log log = LogFactory.getLog(AnnotationDocumentService.class);

	@Context
	protected CoreSession session;

	@Context
	protected AutomationService automationService;

	@Param(name = "uid", required = false)
	protected String uid;
	
	@Param(name = "path", required = false)
	protected String path;

	@Param(name = "tags", required = true)
	protected String tags;

	@Param(name = "username", required = false)
	protected String username;

	@Param(name = "timecodes", required = false)
	protected Properties timecodes;

	@OperationMethod
	public DocumentModel run() {
		DocumentModel inputDoc = null;
		if (StringUtils.isBlank(path) && StringUtils.isBlank(uid)) {
			return session.getRootDocument();
		} 
		if (!StringUtils.isBlank(uid)){
			    inputDoc = session.getDocument(new IdRef(uid));
		}else{
				inputDoc = session.getDocument(new PathRef(path));
		}
		
		    final String inputId = inputDoc.getId();
			OperationContext ctx = new OperationContext(session);
			ctx.setInput(inputDoc);
			Map<String, Object> params = new HashMap<>();
			params = new HashMap<>();
			params.put("tags", tags);
			// add tags to document
			try {
				inputDoc = (DocumentModel) automationService.run(ctx, "Services.TagDocument", params);
			} catch (OperationException e) {
				e.printStackTrace();
			}
			session.saveDocument(inputDoc);
			// for each tag: retrieve tagging relation and set time codes
			/*Arrays.asList(tags.split(",")).stream().forEach(tag -> {

				String query = String.format("SELECT * FROM Tag WHERE tag:label = '%s'", tag);
				DocumentModelList tagDocList = (DocumentModelList) session.query(query);
				log.info("Documetn tag list sixe: " + tagDocList.size());
				DocumentModel tagDoc = tagDocList.get(0);
				String tagId = tagDoc.getId();
				query = String.format("SELECT * FROM Tagging WHERE relation:target='%s' AND relation:source='%s'",
						tagId, inputId);

				DocumentModelList taggingDocList = (DocumentModelList) session.query(query);
				log.info("Documetn tagging list sixe: " + taggingDocList.size());
				DocumentModel taggingDoc = taggingDocList.get(0);
				taggingDoc.setPropertyValue("relation:timecode", timecodes.get(tag));
				session.saveDocument(taggingDoc);
			});*/
			List<String> tagList=Arrays.asList(tags.split(","));
			for(String tag : tagList){
				String query = String.format("SELECT * FROM Tag WHERE tag:label = '%s'", tag);
				DocumentModelList tagDocList = (DocumentModelList) session.query(query);
				log.info("Documetn tag list sixe: " + tagDocList.size());
				DocumentModel tagDoc = tagDocList.get(0);
				String tagId = tagDoc.getId();
				query = String.format("SELECT * FROM Tagging WHERE relation:target='%s' AND relation:source='%s'",
						tagId, inputId);

				DocumentModelList taggingDocList = (DocumentModelList) session.query(query);
				log.info("Documetn tagging list sixe: " + taggingDocList.size());
				DocumentModel taggingDoc = taggingDocList.get(0);
				taggingDoc.setPropertyValue("relation:timecode", timecodes.get(tag));
				session.saveDocument(taggingDoc);
			}
			return inputDoc;
		}
}
