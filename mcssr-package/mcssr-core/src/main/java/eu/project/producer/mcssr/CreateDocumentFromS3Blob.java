package eu.project.producer.mcssr;

import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.repository.RepositoryManager;
import org.nuxeo.ecm.core.blob.BlobManager;
import org.nuxeo.ecm.core.blob.BlobProvider;
import org.nuxeo.ecm.core.blob.binary.Binary;
import org.nuxeo.ecm.core.blob.binary.BinaryBlob;
import org.nuxeo.runtime.api.Framework;

/**
 *
 */
@Operation(id=CreateDocumentFromS3Blob.ID, category=Constants.CAT_DOCUMENT, label="CreateDocumentFromS3Blob", description="Describe here what your operation does.")
public class CreateDocumentFromS3Blob {

    public static final String ID = "Document.CreateDocumentFromS3Blob";
    
    private static final org.apache.commons.logging.Log log = LogFactory.getLog(CreateDocumentFromS3Blob.class);
    
    @Context
    protected CoreSession session;

    @Param(name = "filename")
    protected String filename;
    
    @Param(name = "fileType")
    protected String fileType;

    @Param(name = "mimeType")
    protected String mimeType;

    @Param(name = "digest")
    protected String digest;

    @Param(name = "length")
    protected Long length;

    @OperationMethod
    public DocumentModel run(DocumentModel doc) {
    	if (filename == null) {
            filename = "Untitled";
        }
    	Framework.getLocalService(RepositoryManager.class).getRepositories().forEach(r->log.info("Repository "+r));
    	Framework.getLocalService(BlobManager.class).getBlobProviders().forEach((a,v)->log.info("Key:"+a+" Value: "+v));
    	BlobProvider bp=Framework.getLocalService(BlobManager.class).getBlobProvider(Framework.getLocalService(RepositoryManager.class).getDefaultRepositoryName());
    	Binary binary=bp.getBinaryManager().getBinary(digest);
    	log.info("Binary provider id="+binary.getBlobProviderId());
    	BinaryBlob bb=new BinaryBlob(binary, digest, filename, mimeType, "UTF-8", digest, length);
    	DocumentModel dm=session.createDocumentModel(doc.getPathAsString(), filename, fileType);
    	//dm=session.createDocument(dm);
    	dm.setPropertyValue("file:content", bb);
    	dm.setPropertyValue("dc:title", filename);
    	
    	return session.createDocument(dm);
    }
}
