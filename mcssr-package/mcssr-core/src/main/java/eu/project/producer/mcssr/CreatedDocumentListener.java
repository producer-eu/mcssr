package eu.project.producer.mcssr;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DataModel;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.ecm.core.api.NuxeoException;
import org.nuxeo.ecm.core.event.Event;
import org.nuxeo.ecm.core.event.EventContext;
import org.nuxeo.ecm.core.event.EventListener;
import org.nuxeo.ecm.core.event.impl.DocumentEventContext;
import org.nuxeo.ecm.platform.tag.TagService;
import org.nuxeo.runtime.api.Framework;

import com.amazonaws.util.IOUtils;

import eu.project.producer.mcssr.service.DocumentService;
import eu.project.producer.mcssr.service.McssrTagService;
import eu.project.producer.mcssr.service.Vpt360Service;

public class CreatedDocumentListener implements EventListener {

	protected final List<String> handled = Arrays.asList("aboutToCreate", "documentCreated",
			"beforeDocumentModification", "documentRemoved");

	private static final Log log = LogFactory.getLog(CreatedDocumentListener.class);

	private static final String srptImportEndpoint = Framework.getProperty("srpt.import.service");

	private static final String srptDeleteEndpoint = Framework.getProperty("srpt.delete.service");
	
	private static final String srptApiKey = Framework.getProperty("srpt.api.key");

	public static final int IMPORT_MODE = 1;

	public static final int DELETE_MODE = 2;

	@Context
	protected CoreSession session;

	@Context
	private DocumentService documentService = Framework.getService(DocumentService.class);

	@Context
	private Vpt360Service vpt360Service = Framework.getService(Vpt360Service.class);

	@Context
	private McssrTagService mcssrTagService = Framework.getService(McssrTagService.class);

	public boolean acceptEvent(Event event) {
		return handled.contains(event.getName());
	}

	@Override
	public void handleEvent(Event event) {
		log.debug(event.getName() + " Event Fired");
		EventContext ctx = event.getContext();
		if (!(ctx instanceof DocumentEventContext)) {
			return;
		}
		if (acceptEvent(event)) {
			DocumentEventContext docCtx = (DocumentEventContext) ctx;
			DocumentModel doc = docCtx.getSourceDocument();
			session = doc.getCoreSession();
			if (!documentService.isMultimediaContent(doc)) {
				return;
			}
			log.info("ACCEPTED " + event.getName());
			switch (event.getName()) {
			case "aboutToCreate":
				checkTitleOnCreation(doc, event);
				documentService.downloadBlobFromUrl(doc, ctx);
				break;
			case "documentCreated":
				if (documentService.is360Video(doc)) {
					vpt360Service.create360VideoContent(doc);
				}
				break;
			case "beforeDocumentModification":
				checkTitleOnModification(doc, event);
				if (doc.getType().equals("Video")) {
					log.debug("Doc srtp Status Value" + doc.getProperty("ann:Annotation/srptStatus").getValue());
					log.debug("Doc srtp Status from DB Value" + doc.getCoreSession().getDocument(doc.getRef())
							.getProperty("ann:Annotation/srptStatus").getValue());
					handleDocumentModified(doc, ctx);
				}
				break;
			case "documentRemoved":
				if (doc.getType().equals("Video")) {
					deleteVideoFromSrpt(doc);
				}
				break;
			/*
			 * case "documentModified": if (doc.getType().equals("Video")) {
			 * handleDocumentModified(doc, ctx); } break;
			 */
			default:
				break;
			}

			// ctx.getCoreSession().saveDocument(doc);
			log.info("PROCESSED event " + event.getName());
		}
	}

	private void checkTitleOnCreation(DocumentModel doc, Event event) {
		String title = (String) doc.getProperty("dc:title").getValue();
		log.debug("Title of the source document to be created: " + title);
		String query = String.format("SELECT * FROM Document WHERE dc:title='%s'", title);
		DocumentModelList titleDocList = (DocumentModelList) session.query(query);
		if (titleDocList.size() > 0) {
			event.markBubbleException();
			throw new NuxeoException("Exists another document with the same title, try with another title!");
		}
	}

	private void checkTitleOnModification(DocumentModel doc, Event event) {
		log.debug("Title of the source document to be modified: " + doc.getProperty("dc:title").getValue());
		log.debug("Title of the loaded session document to be modified: "
				+ doc.getCoreSession().getDocument(doc.getRef()).getProperty("dc:title").getValue());
		String q = String.format("SELECT * FROM Document WHERE dc:title='%s'", doc.getProperty("dc:title").getValue());
		DocumentModelList tDocList = (DocumentModelList) session.query(q);
		if (tDocList.size() > 0) {
			DocumentModel d = tDocList.get(0);
			log.debug("Id of the source document to be modified: " + doc.getId());
			log.debug("Id of the loaded session document to be modified: " + d.getId());
			if (!d.getId().equals(doc.getId())) { // if document
													// modified differ
													// from the current
				event.markBubbleException();
				throw new NuxeoException("Exists another document with the same title, try with another title!");
			}
		}

	}

	private boolean hasDirtiesFields(DocumentModel doc) {
		for (DataModel model : doc.getDataModels().values()) {
			if (model.isDirty()) {
				return true;
			}
		}
		return false;
	}

	private synchronized boolean isDocumentPropertyDirty(DocumentModel doc, String xpath) {
		// return doc.getProperty(xpath).isDirty();
		for (DataModel model : doc.getDataModels().values()) {
			if (model.isDirty()) {
				Collection<String> dirtyFieldNames = model.getDirtyFields();
				log.debug("The model " + model.getSchema() + " is dirty");
				log.debug(model.toString());
				for (String fieldName : dirtyFieldNames) {
					log.debug("Dirty fieldname: " + fieldName);
					log.debug("Xpath: " + xpath);
					log.debug("Is property xpath dirty: " + doc.getProperty(xpath).isDirty());
					log.debug("Is property fieldname dirty: " + doc.getProperty(fieldName).isDirty());
					if (fieldName.equals(xpath))
						return true;
				}
			}
		}
		return false;
	}

	private void handleDocumentModified(DocumentModel doc, EventContext ctx) {
		log.debug("Document modifed event: TITLE " + doc.getPropertyValue("dc:title"));
		// unfortunately Nuxeo doesn't recognize as dirty the Property
		// vid:info/duration
		if (doc.getProperty("vid:info/duration").getValue() != null && !doc.getCoreSession().getDocument(doc.getRef())
				.getProperty("ann:Annotation/srptStatus").getValue().equals("SENT")) {
			log.info("Updated vid:info property, import content to SRPT");
			importVideoToSrpt(doc);
			if (documentService.is360Video(doc)) {
				vpt360Service.startPreRenderingOf360VideoContent(doc);
			}
			return;
		}
		if (isDocumentPropertyDirty(doc, "ann:Annotation")) {
			createTags(doc);
			updateVideoAnnotationToSrpt(doc);
		}
	}

	private void updateVideoAnnotationToSrpt(DocumentModel doc) {
		JSONObject jsonToSend = new JSONObject();
		JSONObject video = createVideoRequestToSrpt(doc);
		JSONObject annotations = createAnnotationRequestToSrpt(doc);
		try {
			video.accumulate("annotations", annotations);
			jsonToSend.accumulate("video", video);
		} catch (JSONException e) {
			e.printStackTrace();
			String errMsg = "Error on creating json request to Srpt";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
		}

		// since to a Bug of Nuxeo causing resetting the following fileds, I
		// will re-update loading it from DB
		log.debug("Doc srtp Status Value" + doc.getProperty("ann:Annotation/srptStatus").getValue());
		log.debug("Doc srtp Status from DB Value"
				+ doc.getCoreSession().getDocument(doc.getRef()).getProperty("ann:Annotation/srptStatus").getValue());
		doc.setPropertyValue("ann:Annotation/srptStatus",
				doc.getCoreSession().getDocument(doc.getRef()).getProperty("ann:Annotation/srptStatus").getValue());
		doc.setPropertyValue("ann:Annotation/srptMessage",
				doc.getCoreSession().getDocument(doc.getRef()).getProperty("ann:Annotation/srptMessage").getValue());
		doc.setPropertyValue("ann:Annotation/srptLastUpdate",
				doc.getCoreSession().getDocument(doc.getRef()).getProperty("ann:Annotation/srptLastUpdate").getValue());

		sendContentToSrpt(jsonToSend, IMPORT_MODE, doc);
	}

	private void createTags(DocumentModel doc) {
		if (doc.getPropertyValue("ann:Annotation") != null) {
			mcssrTagService.convertAnnotationToTagOnDocument(doc);

		}

	}

	private void deleteVideoFromSrpt(DocumentModel doc) {
		JSONArray videos = new JSONArray();
		JSONObject shared_id = new JSONObject();
		JSONObject root = new JSONObject();

		try {
			shared_id.accumulate("shared_id", doc.getId());

			videos.put(0, shared_id);

			root.put("videos", videos);
		} catch (JSONException e) {
			log.error("Error on creating request to delete video on Srpt!");
		}

		sendContentToSrpt(root, DELETE_MODE, doc);
	}

	private void importVideoToSrpt(DocumentModel doc) {
		JSONObject jsonToSend = new JSONObject();
		JSONObject video = createVideoRequestToSrpt(doc);
		if (video == null) { //
			return;
		}
		try {
			jsonToSend.accumulate("video", video);
		} catch (JSONException e) {
			log.error("Error during the creation of video Request to Import on Srpt");
		}
		if (sendContentToSrpt(jsonToSend, IMPORT_MODE, doc)) {
			String okMsg = "The Video content has been sent corretly to SRPT";
			log.info(okMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(okMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("SENT");
		}

	}

	private JSONObject createVideoRequestToSrpt(DocumentModel doc) {
		try {
			JSONObject video = new JSONObject();

			String shared_id = doc.getId();
			String source = doc.getPropertyValue("dc:source") != null ? doc.getPropertyValue("dc:source").toString()
					: "";
			String title = doc.getPropertyValue("dc:title") != null ? doc.getPropertyValue("dc:title").toString() : "";
			String path = doc.getPathAsString();
			String description = doc.getPropertyValue("dc:description") != null
					? doc.getPropertyValue("dc:description").toString() : "";
			if (doc.getProperty("vid:info").get("duration") != null) {
				Serializable duration = doc.getProperty("vid:info").getValue("duration");
				video.put("duration", duration);
			}

			String tags = Framework.getLocalService(TagService.class)
					.getDocumentTags(doc.getCoreSession(), doc.getId(), null).stream().map(t -> t.getLabel())
					.collect(Collectors.joining(","));

			video.put("shared_id", shared_id);

			video.put("source", source);
			video.put("title", title);
			video.put("path", path);
			video.put("description", description);

			video.put("tags", tags);

			return video;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	private JSONObject createAnnotationRequestToSrpt(DocumentModel doc) {
		Serializable annotation = null;
		JSONObject annotationToSave = null;

		annotation = doc.getPropertyValue("ann:Annotation").toString();

		log.debug("annotation:" + annotation);

		JSONObject jsonAnnotation = null;
		try {
			jsonAnnotation = new JSONObject(
					documentService.getJsonOfProperty(doc.getProperty("ann:Annotation")).toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String annotationStatus = jsonAnnotation.optJSONObject("ann:Annotation").optString("annotationStatus");

		log.debug("annotationStatus:" + annotationStatus);
		if (annotationStatus.equals("ANNOTATED")) {

			JSONObject transcription = jsonAnnotation.optJSONObject("ann:Annotation").optJSONObject("transcription");

			JSONArray objectdetection = jsonAnnotation.optJSONObject("ann:Annotation").optJSONArray("objectdetection");

			JSONArray facedetection = jsonAnnotation.optJSONObject("ann:Annotation").optJSONArray("facedetection");

			annotationToSave = new JSONObject();

			try {
				annotationToSave.accumulate("transcription", transcription);
				annotationToSave.put("objectdetection", objectdetection);
				annotationToSave.put("facedetection", facedetection);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			log.debug("annotationToSave" + annotationToSave);
			return annotationToSave;
		} else {
			return null;
		}
	}

	private boolean sendContentToSrpt(JSONObject jsonToSend, int mode, DocumentModel doc) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = null;

		if (IMPORT_MODE == mode) {
			httpPost = new HttpPost(srptImportEndpoint);
		} else if (DELETE_MODE == mode) {
			httpPost = new HttpPost(srptDeleteEndpoint);
		} else {
			String errMsg = "No Srpt mode value: " + mode + " found!";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");

			return false;
		}

		StringEntity entity = null;

		try {
			entity = new StringEntity(jsonToSend.toString());
		} catch (UnsupportedEncodingException e) {
			String errMsg = "Error on parsing Body Request to Srpt";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");

			return false;
		}
		try {
			log.info("Response body sending to SRPT: \n" + IOUtils.toString(entity.getContent()));
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		httpPost.setHeader("Api-Key", srptApiKey);

		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpPost);
		} catch (IOException e) {
			String errMsg = "Error on executing request towards Srpt Tool";
			log.error(errMsg);
			log.debug(e.getMessage());
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return false;
		}
		log.info("Response from SRPT:" + response.getStatusLine());
		HttpEntity responseEntity = response.getEntity();
		
		int code = response.getStatusLine().getStatusCode();
		log.info("Response code from SRPT:" + code);
		try {
			log.info("Response body from SRPT: \n" + IOUtils.toString(responseEntity.getContent()));
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			httpclient.close();
			return true;
		} catch (IOException e) {
			String errMsg = "Error on closing the http connection with Srpt Tool";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			return false;
		}

	}

}
