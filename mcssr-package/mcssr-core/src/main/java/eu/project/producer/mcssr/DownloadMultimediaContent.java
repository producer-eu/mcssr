package eu.project.producer.mcssr;

import org.apache.commons.lang3.StringUtils;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationContext;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.core.api.*;
import org.nuxeo.runtime.transaction.TransactionHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Operation(id=DownloadMultimediaContent.ID, category=Constants.CAT_DOCUMENT, label="DownloadMultimediaContent", description="Describe here what your operation does.")
public class DownloadMultimediaContent {

    public static final String ID = "Document.DownloadMultimediaContent";
    
    private static final Log log = LogFactory.getLog(DownloadMultimediaContent.class);

    @Context
    protected CoreSession session;

    @Context
    protected AutomationService automationService;

    @Param(name = "url", required = true)
    protected String url;

    @Param(name = "fileName", required = false)
    protected String fileName;

    @Param(name = "path", required = true)
    protected String path;

    @Param(name = "type", required = false)
    protected String type;

    @Param(name = "mimeType", required = false)
    protected String mimeType;

    @Param(name = "encoding", required = false)
    protected String encoding;
    
    @Param(name = "tags", required = false)
    protected String tags;
    
    @Param(name = "description", required = false)
    protected String description;
    
    @Param(name = "license", required = false)
    protected String license;
    
    @Param(name = "repository", required = false)
    protected String repository;

    @OperationMethod
    public DocumentModel run() {
        if (!StringUtils.isBlank(path) && !StringUtils.isBlank(url)) {
        	

        	TransactionHelper.commitOrRollbackTransaction();
            TransactionHelper.startTransaction();
            
            DocumentModel doc=session.createDocumentModel(path,fileName,type);
            doc.setPropertyValue("dc:title", fileName);
            doc.setPropertyValue("dc:description", description);
            doc.setPropertyValue("dc:source", repository);


            doc = session.createDocument(doc);
        
            log.info("createDocumentModel done");
            System.out.println("*************Download Content***********");
            IdRef idRef=new IdRef(doc.getId());
            doc=session.getDocument(idRef);

            OperationContext ctx = new OperationContext(session);
            Map<String, Object> params = new HashMap<>();
            params.put("file", url);
            params.put("encoding", encoding);
            params.put("filename", fileName);
            params.put("mime-type", mimeType);
            Blob blob = null;
            try {
                blob = (Blob) automationService.run(ctx, 	"Blob.CreateFromURL", params);
            } catch (OperationException e) {
                e.printStackTrace();
            }
            
            log.info("Blob.CreateFromURL done");

            ctx=new OperationContext(session);
            ctx.setInput(blob);
            params=new HashMap<>();
            params.put("document", doc);

            try {
                blob = (Blob) automationService.run(ctx, 	"Blob.AttachOnDocument", params);
            } catch (OperationException e) {
                e.printStackTrace();
            }

            log.info("Blob.AttachOnDocument done");

            ctx=new OperationContext(session);
            ctx.setInput(doc);
            params=new HashMap<>();
            params.put("tags", tags);

            try {
                doc = (DocumentModel) automationService.run(ctx, 	"Services.TagDocument", params);
            } catch (OperationException e) {
                e.printStackTrace();
            }
            
            //save the document only at the end of chain
            doc = session.saveDocument(doc);
            session.save();
            log.info("Services.TagDocument done - END!");
            
            return doc;
        } else {
            return session.getDocument(new PathRef(path));
        }

    }
    

}
