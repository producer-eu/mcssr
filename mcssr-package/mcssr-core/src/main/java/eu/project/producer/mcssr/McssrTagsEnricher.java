package eu.project.producer.mcssr;

import static org.nuxeo.ecm.core.io.registry.reflect.Instantiations.SINGLETON;
import static org.nuxeo.ecm.core.io.registry.reflect.Priorities.REFERENCE;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.ecm.core.io.marshallers.json.enrichers.AbstractJsonEnricher;
import org.nuxeo.ecm.core.io.registry.reflect.Setup;
import org.nuxeo.ecm.platform.tag.Tag;
import org.nuxeo.ecm.platform.tag.TagService;
import org.nuxeo.runtime.api.Framework;

@Setup(mode = SINGLETON, priority = REFERENCE)
public class McssrTagsEnricher extends AbstractJsonEnricher<DocumentModel> {

	// The enricher is called using enrichers.document: mcssrTags
	public static final String NAME = "mcssrTags";

	private static final Log log = LogFactory.getLog(McssrTagsEnricher.class);

	@Context
	protected CoreSession session;

	@Context
	protected TagService tagService;

	private ObjectMapper o;

	public McssrTagsEnricher() {
		super(NAME);
	}

	@Override
	public void write(JsonGenerator jg, DocumentModel doc) throws IOException {
		session = doc.getCoreSession();
		tagService = Framework.getService(TagService.class);
		o = new ObjectMapper();
		ArrayNode tagsJsonObject = addTagsAsJson(doc);
		jg.writeFieldName(NAME);
		jg.writeStartArray();
		tagsJsonObject.forEach(r->{
			try {
				jg.writeObject(r);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		jg.writeEndArray();
	}

	private ArrayNode addTagsAsJson(DocumentModel doc) {

		// TODO add tag
		ArrayNode tagsArray = o.createArrayNode();
		tagService.getDocumentTags(session, doc.getId(), null).stream().map(r -> createTagObject(doc, r))
				.forEach(a -> tagsArray.add(a));
		return tagsArray;
	}

	private ObjectNode createTagObject(DocumentModel doc, Tag r) {
		ObjectNode tagObject = o.createObjectNode();
		String query = String.format("SELECT * FROM Tag WHERE tag:label = '%s'", r.getLabel());
		DocumentModelList tagDocList = (DocumentModelList) session.query(query);
		//log.info("Tag query: " + query);
		//log.info("Document tag list " + r.getLabel() + " size: " + tagDocList.size());
		DocumentModel tagDoc = tagDocList.get(0);
		query = String.format("SELECT * FROM Tagging WHERE relation:target='%s' AND relation:source='%s'",
				tagDoc.getId(), doc.getId());
		DocumentModelList taggingDocList = (DocumentModelList) session.query(query);
		//log.info("Tagging query: " + query);
		//log.info("Document tagging list with uid:" + doc.getId() + " size: " + taggingDocList.size());
		DocumentModel taggingDoc = taggingDocList.get(0);
		String timecodes = taggingDoc.getPropertyValue("relation:timecode") == null ? null
				: taggingDoc.getPropertyValue("relation:timecode").toString();
		ArrayNode jsonArray = o.createArrayNode();
		if (timecodes != null) {
			String[] tcArray = timecodes.split(",");
			for (String s : tcArray) {
				jsonArray.add(s);
			}
		}
		tagObject.put(r.getLabel(), jsonArray);
		return tagObject;

	}

}
