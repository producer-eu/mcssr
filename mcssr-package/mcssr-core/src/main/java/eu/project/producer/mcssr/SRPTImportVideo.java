package eu.project.producer.mcssr;


import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.IdRef;

import eu.project.producer.mcssr.service.SrptServiceImpl;

/**
 *
 */
@Operation(id = SRPTImportVideo.ID, category = Constants.CAT_SERVICES, label = "SRPTImportVideo", description = "Service to Import video document to SRPT.")
public class SRPTImportVideo {

	public static final String ID = "Services.SRPTImportVideo";

	private static final Log log = LogFactory.getLog(SyncSRPT.class);

	@Context
	protected CoreSession session;

	@Context
	protected AutomationService automationService;
	
	@Param(name = "uid", required = false)
	protected String uid;

	@OperationMethod
	public int run() throws OperationException, IOException {
		
		DocumentModel inputDoc = null;
		SrptServiceImpl srptservice = new SrptServiceImpl();
		if (!StringUtils.isBlank(uid)){
			    inputDoc = session.getDocument(new IdRef(uid));
			    return srptservice.importVideoToSrpt(inputDoc);
		}
		
		return 0;

	}
	

	
}
