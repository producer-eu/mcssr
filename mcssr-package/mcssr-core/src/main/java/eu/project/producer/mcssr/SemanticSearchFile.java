package eu.project.producer.mcssr;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.util.BlobList;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.runtime.api.Framework;

import java.io.File;

/**
 *
 */
@Operation(
        id= SemanticSearchFile.ID,
        category=Constants.CAT_DOCUMENT,
        label="Semantic Search - File",
        description="Search files for keywords to use to find related media")
public class SemanticSearchFile {

    private static final Log log = LogFactory.getLog(AATUploadingDB.class);

    public static final String ID = "Document.SemanticSearchFile";

    @Context
    protected CoreSession session;

    @Context
    private SemanticSearchService semanticSearchService = Framework.getService(SemanticSearchServiceImpl.class);

    @OperationMethod
    public String run(BlobList blobs) throws Exception {
        log.debug("Semantic Search - File: BlobList");
        File file = blobs.get(0).getFile();
        log.debug("Semantic Search - File path: " + file.getAbsolutePath());
        String res = semanticSearchService.getFileAnnotations(blobs.get(0).getFile());
        log.debug("Semantic Search - File content: " + res);
        return res;
    }

}