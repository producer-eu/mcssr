package eu.project.producer.mcssr;

import java.io.File;

public interface SemanticSearchService {

    String getFileAnnotations(File file) throws Exception;

    String getUrlAnnotations(String url) throws Exception;

}
