package eu.project.producer.mcssr;

import eu.project.producer.mcssr.exception.SemanticSearchException;
import gate.*;
import gate.creole.ExecutionException;
import gate.util.persistence.PersistenceManager;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.nuxeo.common.Environment;
import org.nuxeo.runtime.model.DefaultComponent;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * This class implements a semantic search service that uses the GATE Embedded
 * library to analyze text files and extract meaningful Annotations. The
 * original GATE application used is the ANNIE default application, but the
 * application itself can be modified with the GATE software and reloaded as
 * necessary. Furthermore, the semantic analyzer is compatible with URLs, and is
 * able to analyze the HTM content of web-pages.
 *
 * Update: The ANNIE application used is defined in the "ANNIE_with_extras.gapp"
 * file, and has some additional features like basic animals annotation
 * capabilities.
 *
 * The Class is designed as a Singleton since there needs to be only one
 * initialized Semantic Search Service at any given moment.
 */
public class SemanticSearchServiceImpl extends DefaultComponent implements SemanticSearchService {

	private List<String> annotationsTypesToWrite = null;

	private CorpusController application;

	private String encoding = null;

	private static final Log log = LogFactory.getLog(SemanticSearchServiceImpl.class);

	public SemanticSearchServiceImpl() throws Exception {
		Path gatePath = Paths.get(Environment.getDefault().getConfig().toPath().toString() + File.separator +
                "GATE");
		Path pluginsPath = Paths.get(Environment.getDefault().getConfig().toPath().toString() + File.separator +
				"GATE" + File.separator + "plugins");

		log.debug("GATE path: " + gatePath.toString());
		log.debug("plugins path: " + pluginsPath.toString());
		// retrieve GATE home and GATE plugins
		if (Gate.getGateHome() == null)
			Gate.setGateHome(gatePath.toFile());
		if (Gate.getPluginsHome() == null)
			Gate.setPluginsHome(pluginsPath.toFile());

		// initialize GATE
		Gate.init();

		// load the application from the default plugins
		File pluginsHome = Gate.getPluginsHome();
		File anniePlugin = new File(pluginsHome, "ANNIE");
		File applicationFile = new File(anniePlugin, "ANNIE_with_extras.gapp");

		// default annotations to write out
		annotationsTypesToWrite = new ArrayList<>();
		annotationsTypesToWrite.add("Person");
		annotationsTypesToWrite.add("Date");
		annotationsTypesToWrite.add("Location");
		annotationsTypesToWrite.add("Animal");
		annotationsTypesToWrite.add("Organization");

		// load the saved GATE application
		application = (CorpusController) PersistenceManager.loadObjectFromFile(applicationFile);

	}

	@Override
	public String getFileAnnotations(File file) throws Exception {

		// Create a Corpus to be processed by the application
		Corpus corpus = Factory.newCorpus("BatchProcessApp Corpus");
		application.setCorpus(corpus);

		// load the document (using the specified encoding if one was given)
		Document doc = Factory.newDocument(file.toURI().toURL(), encoding);

		// put the document in the corpus
		corpus.add(doc);

		// run the application
		try {
			application.execute();
		} catch (ExecutionException e) {
			throw throwAsError("Your document could not be processed; usually that means that no words could "
					+ "be found or that your file is empty.");
		}

		// remove the document from the corpus again
		corpus.clear();

		return processResult(doc);

	}

	@Override
	public String getUrlAnnotations(String url) throws Exception {

		// Create a Corpus to be processed by the application
		Corpus corpus = Factory.newCorpus("BatchProcessApp Corpus");
		application.setCorpus(corpus);
		FeatureMap params = Factory.newFeatureMap();

		// get the doc from the URL
		URL u;
		Document doc;
		try {
			u = new URL(url);
			params.put("sourceUrl", u);
			doc = (Document) Factory.createResource("gate.corpora.DocumentImpl", params);
		} catch (NullPointerException e) {
			throw throwAsError("The URL you submitted cannot be reached.");
		} catch (Exception e) {
			if (url.equals(""))
				throw throwAsError("The URL you submitted is empty");
			else
				throw throwAsError("\"" + url + "\" is not an URL...");
		}

		// put the document in the corpus
		corpus.add(doc);

		// run the application
		try {
			application.execute();
		} catch (ExecutionException e) {
			throw throwAsError("Your document could not be processed; usually that means that no words could "
					+ "be found or that your file is empty.");
		}

		// remove the document from the corpus again
		corpus.clear();

		return processResult(doc);

	}

	private String processResult(Document doc) throws Exception {

		// use Jackson to create the JSON representation of the annotations
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode jsonRoot = mapper.createObjectNode();

		// Create a temporary Set to hold the annotations we wish to write out
		Set<Annotation> annotationSet = new HashSet<>();

		// get all annotations from the document
		AnnotationSet defaultAnnotations = doc.getAnnotations();
		for (String annotationType : annotationsTypesToWrite) {
			// extract all the annotations of each requested type and add them to the temporary set
			AnnotationSet annotationsOfThisType = defaultAnnotations.get(annotationType);
			if (annotationsOfThisType != null) {
				jsonRoot.put(annotationType, mapper.createArrayNode());
				annotationSet.addAll(annotationsOfThisType);
			}
		}

		// Release the document, as it is no longer needed; we only work with the retrieved annotations from now on
		Factory.deleteResource(doc);

		// extract the necessary information from the annotations and create the JSON object
		ArrayList<Integer> matched = new ArrayList<>(); // list to prevent duplicate annotations

		// Map of annotations IDs, content and type
		HashMap<Integer, String> idTypeMap = new HashMap<>();
		HashMap<Integer, ArrayList<String>> idContentMap = new HashMap<>();
		HashMap<Integer, Integer> idOccurrenceMap = new HashMap<>();

		for (Annotation annotation : annotationSet) {

			// annotation features
			FeatureMap annotationFeatures = annotation.getFeatures();

			// annotation content
			String annotationContent = doc.getContent()
					.getContent(annotation.getStartNode().getOffset(), annotation.getEndNode().getOffset()).toString();

			// get list of annotation matches
			@SuppressWarnings("unchecked") // "matches" field is stored as list of integers by GATE
			List<Integer> matchesList = (List<Integer>) annotationFeatures.get("matches");

			// annotation occurrences
			int annotationOccurrences = (matchesList != null) ? matchesList.size() : 1;

			if (matchesList != null) {
				// annotation has other matches: compare it to the expected matches already seen
				if (matched.contains(annotation.getId())) {
					// annotation was matched by another one: find that annotation
					for (Integer key : idContentMap.keySet()) {
						if (matchesList.contains(key)) {
							// found the annotation: add it to the content map for later processing
                            idContentMap.get(key).add(capitalize(annotationContent));
						} // that key is not in the annotation matches
					}
				} else {
					// annotation is the first of its kind, update all data structures
                    // content
                    ArrayList<String> contentList = new ArrayList<>();
                    contentList.add(capitalize(annotationContent));
					idContentMap.put(annotation.getId(), contentList);
					// occurrences
					idOccurrenceMap.put(annotation.getId(), annotationOccurrences);
					// type
					idTypeMap.put(annotation.getId(), annotation.getType());
					matched.addAll(matchesList);
				}
			} else {
				// annotation is single: add it directly to the JSON object
				this.addAnnotationToJson(jsonRoot, annotation.getType(), capitalize(annotationContent), annotationOccurrences,
						mapper);
			}

		}

		// add all the remaining multiple-occurrence annotations to the JSON
		for (int id : idTypeMap.keySet()) {
			String type = idTypeMap.get(id);
			String content = this.mostCommon(idContentMap.get(id));
			int occurrences = idOccurrenceMap.get(id);
			addAnnotationToJson(jsonRoot, type, content, occurrences, mapper);
		}

		return mapper.defaultPrettyPrintingWriter().writeValueAsString(jsonRoot);

	}

    private void addAnnotationToJson(ObjectNode root, String type, String content, int occurrences,
			ObjectMapper mapper) {

		// create the JSON node
		ObjectNode jsonAnnotation = mapper.createObjectNode();

		// set annotation content
		jsonAnnotation.put("content", content);

		// set annotation occurrence
		jsonAnnotation.put("occurrences", occurrences);

		// add annotation to the appropriate list of the JSON root
		ArrayNode jsonAnnotationsArray = ((ArrayNode) root.get(type));
		this.insertNodeByOccurrences(jsonAnnotationsArray, jsonAnnotation, occurrences);

	}

	private void insertNodeByOccurrences(ArrayNode array, ObjectNode element, int occurrences) {

		if (array.size() != 0) {
			for (int i = 0; i < array.size(); i++) {
				int otherOccurrences = array.get(i).get("occurrences").getValueAsInt();
				if (otherOccurrences <= occurrences) {
					array.insert(i, element);
					break;
				}
				if (i == array.size() - 1)
					array.add(element);
			}
		} else
			array.add(element);

	}

    private <T> T mostCommon(List<T> list) {
        Map<T, Integer> map = new HashMap<>();

        for (T t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        int max = -1;
        T e = null;
        for (T key : map.keySet()) {
            int n = map.get(key);
            if (n > max) {
                max = n;
                e = key;
            }
        }

        return e;
    }

	private SemanticSearchException throwAsError(String message) {

		return new SemanticSearchException(message);

	}

	private String capitalize(String string) {
        return WordUtils.capitalizeFully(string);
    }

}
