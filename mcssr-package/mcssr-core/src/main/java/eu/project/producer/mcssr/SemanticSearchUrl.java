package eu.project.producer.mcssr;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.impl.blob.FileBlob;
import org.nuxeo.runtime.api.Framework;

/**
 *
 */
@Operation(
        id=SemanticSearchUrl.ID,
        category=Constants.CAT_DOCUMENT,
        label="Semantic Search - URL",
        description="Search URLs for keywords to use to find related media")
public class SemanticSearchUrl {

    private static final Log log = LogFactory.getLog(AATUploadingDB.class);

    public static final String ID = "Document.SemanticSearchUrl";

    @Context
    protected CoreSession session;

    @Context
    private SemanticSearchService semanticSearchService = Framework.getService(SemanticSearchServiceImpl.class);

    @Param(name = "url")
    protected String url;

    @OperationMethod
    public String run() throws Exception {
        return semanticSearchService.getUrlAnnotations(url);
    }

}