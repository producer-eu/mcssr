package eu.project.producer.mcssr;


import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DataModel;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.ecm.core.api.IdRef;
import org.nuxeo.ecm.platform.tag.TagService;
import org.nuxeo.runtime.api.Framework;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.project.producer.mcssr.service.DocumentService;


/**
 *
 */
@Operation(id = SyncSRPT.ID, category = Constants.CAT_SERVICES, label = "SyncSRPT", description = "Service to sync all documents with SRPT.")
public class SyncSRPT {

	public static final String ID = "Services.SyncSRPT";

	private static final Log log = LogFactory.getLog(SyncSRPT.class);

	private static final String srptImportEndpoint = Framework.getProperty("srpt.import.service");

	private static final String srptDeleteEndpoint = Framework.getProperty("srpt.delete.service");
	
	private static final String srptGetAllEndpoint = Framework.getProperty("srpt.getall.service");
	
	private static final String srptImportDEV = "localhost:4444/echo";

	private static final String getmcssrlist= "localhost:4444/mcssrlist";

	
	
	public static final int IMPORT_MODE = 1;

	public static final int DELETE_MODE = 2;
	
	public static final int GETALL_MODE = 3;

	@Context
	protected CoreSession session;

	@Context
	protected AutomationService automationService;

	@OperationMethod
	public DocumentModelList run() throws OperationException, IOException {
		String query = "SELECT * FROM Document where ecm:mixinType='Video'";
		DocumentModelList videolist = (DocumentModelList) session.query(query);
		DocumentModel doc =  null;
	/* remove in PROD */
		List<String> mcssrlist = getmcssrlist();
	/* END */	
		
		List<String> srptlist = getsrptlist();
		List<String> listToUpdate = new ArrayList();
		List<String> listToRemove = new ArrayList();
		
	/* set this code in PROD */
	/*	
	 *  List<String> mcssrlist = new ArrayList(); 
		for (DocumentModel dm : videolist) {
			//updateVideoAnnotationToSrpt(dm);
			//System.out.println("mcssr uid:"+dm.getId());
			mcssrlist.add(dm.getId());
		}
	*/
		log.debug("mcssrvideolist:"+mcssrlist);
		log.debug("PROD srptlist:"+srptlist);
		
		//mcssrlist.removeAll(srptlist);

		listToUpdate = mcssrlist.stream()
				.filter(a->!srptlist.contains(a))
				.collect(Collectors.toList());
	
		
		log.debug("final listToUpdate - items in mcssr but not in srpt:"+listToUpdate);
				
		listToRemove = srptlist.stream()
				.filter(a->!mcssrlist.contains(a))
				.collect(Collectors.toList());
		
		log.debug("final listToRemove - items in srpt but not in mcssr:"+listToRemove);
		
		
		return videolist;
	}
	

	

	
	private void importVideoToSrpt(DocumentModel doc) {
		JSONObject jsonToSend = new JSONObject();
		JSONObject video = createVideoRequestToSrpt(doc);
		if (video == null) { //
			return;
		}
		try {
			jsonToSend.accumulate("video", video);
		} catch (JSONException e) {
			log.error("Error during the creation of video Request to Import on Srpt");
		}
		if (sendContentToSrpt(jsonToSend, IMPORT_MODE, doc)) {
			String okMsg = "The Video content has been sent corretly to SRPT";
			log.info(okMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(okMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("SENT");
			/*
			 * try {
			 * 
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", okMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "SENT"); } catch (IOException e1) {
			 * e1.printStackTrace(); }
			 */
		}

	}

	private JSONObject createVideoRequestToSrpt(DocumentModel doc) {
		try {
			JSONObject video = new JSONObject();

			String shared_id = doc.getId();
			String source = doc.getPropertyValue("dc:source") != null ? doc.getPropertyValue("dc:source").toString()
					: "";
			String title = doc.getPropertyValue("dc:title") != null ? doc.getPropertyValue("dc:title").toString() : "";
			String path = doc.getPathAsString();
			String description = doc.getPropertyValue("dc:description") != null
					? doc.getPropertyValue("dc:description").toString() : "";
			if (doc.getProperty("vid:info").get("duration") != null) {
				Serializable duration = doc.getProperty("vid:info").getValue("duration");
				video.put("duration", duration);
			}

			String tags = Framework.getLocalService(TagService.class)
					.getDocumentTags(doc.getCoreSession(), doc.getId(), null).stream().map(t -> t.getLabel())
					.collect(Collectors.joining(","));

			video.put("shared_id", shared_id);

			video.put("source", source);
			video.put("title", title);
			video.put("path", path);
			video.put("description", description);

			video.put("tags", tags);

			return video;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	
	private List<String> getsrptlist() {
		log.debug("into getsrptlist");
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = null;
		httpGet = new HttpGet(srptGetAllEndpoint);
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpGet);
		} catch (IOException e) {
			String errMsg = "Error on executing request towards Srpt Tool";
			log.error(errMsg);
			log.error(e.getMessage());
		}
		
		HttpEntity entity = response.getEntity();
		String responseString = null;
		try {
			responseString = EntityUtils.toString(entity, "UTF-8");
			//System.out.println("Response responseEntity from SRPT:" + responseString);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		List<String> srptlist = new ArrayList();
	    try {
			JsonNode arrNode = mapper.readTree(responseString);
			if (arrNode.isArray()) {
			    for (final JsonNode objNode : arrNode) {
			    	if(!objNode.get("euscreen").asText().contains("EUS_")) {
			    		log.debug("srpt uid:"+objNode.get("euscreen"));
				        srptlist.add(objNode.get("euscreen").asText());
			    	}
			    }
			}
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return srptlist;
		
	}
	
	
	private List<String> getmcssrlist() {
		log.debug("into getmcssrlist");
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = null;
		httpGet = new HttpGet(getmcssrlist);
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpGet);
		} catch (IOException e) {
			String errMsg = "Error on executing request towards fake MCSSR list ";
			log.debug(errMsg);
			log.debug(e.getMessage());
		}
		
		HttpEntity entity = response.getEntity();
		String responseString = null;
		try {
			responseString = EntityUtils.toString(entity, "UTF-8");
			//System.out.println("Response responseEntity from SRPT:" + responseString);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		List<String> mcssrlist = new ArrayList();
	    try {
			JsonNode docs = mapper.readTree(responseString);
			JsonNode arrNode = docs.get("entries");
			if (arrNode.isArray()) {
			    for (final JsonNode objNode : arrNode) {
			    	if(objNode.get("uid").asText() != null) {
			    		log.debug("mcssr uid:"+objNode.get("uid"));
				        mcssrlist.add(objNode.get("uid").asText());
			    	}
			    }
			}
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return mcssrlist;
		
	}
	
	private synchronized boolean isDocumentPropertyDirty(DocumentModel doc, String xpath) {
		// return doc.getProperty(xpath).isDirty();
		for (DataModel model : doc.getDataModels().values()) {
			if (model.isDirty()) {
				Collection<String> dirtyFieldNames = model.getDirtyFields();
				log.debug("The model " + model.getSchema() + " is dirty");
				log.debug(model.toString());
				for (String fieldName : dirtyFieldNames) {
					log.debug("Dirty fieldname: " + fieldName);
					log.debug("Xpath: " + xpath);
					log.debug("Is property xpath dirty: " + doc.getProperty(xpath).isDirty());
					log.debug("Is property fieldname dirty: " + doc.getProperty(fieldName).isDirty());
					if (fieldName.equals(xpath))
						return true;
				}
			}
		}
		return false;
	}
	

	private boolean sendContentToSrpt(JSONObject jsonToSend, int mode, DocumentModel doc) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = null;

		if (IMPORT_MODE == mode) {
			httpPost = new HttpPost(srptImportEndpoint);
		} else if (DELETE_MODE == mode) {
			httpPost = new HttpPost(srptDeleteEndpoint);
		} else {
			String errMsg = "No Srpt mode value: " + mode + " found!";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return false;
		}

		StringEntity entity = null;

		try {
			entity = new StringEntity(jsonToSend.toString());
		} catch (UnsupportedEncodingException e) {
			String errMsg = "Error on parsing Body Request to Srpt";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return false;
		}
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");

		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpPost);
		} catch (IOException e) {
			String errMsg = "Error on executing request towards Srpt Tool";
			log.error(errMsg);
			log.debug(e.getMessage());
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return false;
		}
		log.info("Response from SRPT:" + response.getStatusLine());
		HttpEntity responseEntity = response.getEntity();
		int code = response.getStatusLine().getStatusCode();
		log.info("Response code from SRPT:" + code);
		try {
			httpclient.close();
			return true;
		} catch (IOException e) {
			String errMsg = "Error on closing the http connection with Srpt Tool";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return false;
		}

	}

}
