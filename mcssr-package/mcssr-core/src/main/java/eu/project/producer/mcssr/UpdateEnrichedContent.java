package eu.project.producer.mcssr;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.automation.jaxrs.io.operations.RestOperationContext;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.IdRef;
import org.nuxeo.ecm.core.api.PropertyException;
import org.nuxeo.ecm.core.api.model.Property;
import org.nuxeo.ecm.core.api.model.PropertyNotFoundException;
import org.nuxeo.ecm.core.api.model.impl.PropertyFactory;
import org.nuxeo.ecm.core.schema.Namespace;
import org.nuxeo.ecm.core.schema.types.ComplexTypeImpl;
import org.nuxeo.ecm.core.schema.types.Field;
import org.nuxeo.ecm.core.schema.types.FieldImpl;
import org.nuxeo.ecm.core.schema.types.QName;
import org.nuxeo.ecm.core.schema.types.SchemaImpl;
import org.nuxeo.ecm.core.schema.types.Type;
import org.nuxeo.ecm.core.schema.types.primitives.BooleanType;
import org.nuxeo.ecm.core.schema.types.primitives.LongType;
import org.nuxeo.ecm.core.schema.types.primitives.StringType;

import com.mongodb.util.JSON;

import eu.project.producer.mcssr.exception.JsonParsingException;
import eu.project.producer.mcssr.exception.MissingFieldException;

/**
 *
 */
/**
 * @author Pasquale Panuccio
 * @email pasquale.panuccio@finconsgroup.com
 *
 */
@Operation(id = UpdateEnrichedContent.ID, category = Constants.CAT_DOCUMENT, label = "UpdateEnrichedContent", description = "Describe here what your operation does.")
public class UpdateEnrichedContent {

	public static final String ID = "Document.UpdateEnrichedContent";

    public static final String SCHEMA_NAME = "annotation";

    public static final SchemaImpl SCHEMA = new SchemaImpl(SCHEMA_NAME, new Namespace("http://www.nuxeo.org/ecm/schemas/enrichedcontent/", "evc"));
    
	private static final Log log = LogFactory.getLog(UpdateEnrichedContent.class);

	@Context
	RestOperationContext context;

	@Context
	protected CoreSession session;

	@Context
	protected AutomationService automationService;

	@Param(name = "enrichedItem", required = true)
	protected String enrichedItem;

	@OperationMethod
	public DocumentModel run() throws OperationException, IOException, JSONException {

		DocumentModel doc = null;

		JSONObject json = null;
		try {
			json = new JSONObject(enrichedItem);
		} catch (JSONException e) {
			JsonParsingException ex = new JsonParsingException("Error on parsing Enriched Item in input");
			//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw ex;
		}
		
		log.info("Body Request coming from IEVC: \n"+json.toString());
		JSONArray itemsArray = null;

		try {
			itemsArray = json.getJSONArray("Items");
		} catch (JSONException e) {
			JsonParsingException ex = new JsonParsingException("Error on parsing the Items array in input");
			//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw ex;
		}
		JSONObject item = null;
		try {
			item = itemsArray.getJSONObject(0);

		} catch (JSONException e1) {
			JsonParsingException ex = new JsonParsingException("Error on parsing the Items array in input");
			//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw ex;
		}

		String uid, pid = null;
		if (item.has("mcssruid")) {
			uid = item.getString("mcssruid");
		} else {
			MissingFieldException ex = new MissingFieldException("Missing mcssruid field on body request");
			//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw ex;
		}
		if (item.has("pid")) {
			pid = item.getString("pid");
		} else {
			MissingFieldException ex = new MissingFieldException("Missing pid field on body request");
			//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw ex;
		}
		
		log.info("Mccsr uid to process: "+uid);
		log.info("IEVC pid to process: "+pid);
		
		doc = session.getDocument(new IdRef(uid));

		int pidIndex = getPidIndex(pid, doc);
		log.debug("Pid index found: "+pidIndex);
		updatePid(pidIndex, pid, doc, item);
		
		Property evcProp=doc.getProperty("evc:enrichedItems");
		evcProp.setValue("Count", evcProp.get("Items").size());
		//updateFieldLongProprerty(evcProp, "ScannedCount", json);
		
		return doc.getCoreSession().saveDocument(doc);

	}

	private void updatePid(int pidIndex, String pid, DocumentModel doc, JSONObject item)
			throws JsonParsingException, MissingFieldException {
			Property itemProperty = null;
			if (pidIndex == -1) { // no pid item is present, create a new one
				itemProperty = doc.getProperty("evc:enrichedItems").get("Items").addEmpty();
			} else { // update pid item
				itemProperty = doc.getProperty("evc:enrichedItems").get("Items").get(pidIndex);
			}
			updateFieldStringProprerty(itemProperty, "uid", item);
			updateFieldStringProprerty(itemProperty, "theta", item);
			updateFieldStringProprerty(itemProperty, "js", item);
			updateFieldStringProprerty(itemProperty, "apexA", item);
			updateFieldBooleanProprerty(itemProperty, "controls", item);
			updateFieldLongProprerty(itemProperty, "created", item);
			updateFieldLongProprerty(itemProperty, "updated", item);
			updateFieldStringProprerty(itemProperty, "name", item);
			updateFieldBooleanProprerty(itemProperty, "loop", item);
			updateFieldLongProprerty(itemProperty, "seekTo", item);
			updateFieldLongProprerty(itemProperty, "phi", item);
			updateFieldBooleanProprerty(itemProperty, "playOnce", item);
			updateFieldBooleanProprerty(itemProperty, "muted", item);
			updateFieldBooleanProprerty(itemProperty, "play", item);
			updateFieldStringProprerty(itemProperty, "pid", item);
			updateFieldBooleanProprerty(itemProperty, "autoplay", item);
			updateFieldBooleanProprerty(itemProperty, "fullscreen", item);
			updateFieldBooleanProprerty(itemProperty, "spinner", item);
			updateFieldStringProprerty(itemProperty, "mcssruid", item);
			updateFieldLongProprerty(itemProperty, "apexB", item);

			if (item.has("source") && !item.isNull("source")) {

				JSONObject source = null;
				try {
					source = item.getJSONObject("source");
				} catch (JSONException e) {
					JsonParsingException ex = new JsonParsingException("Error on parsing source Item in input");
					//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					throw ex;
				}

				Property sourceProperty = itemProperty.get("source");

				updateFieldStringProprerty(sourceProperty, "uid", source);
				updateFieldStringProprerty(sourceProperty, "mcssruid", source);
				updateFieldStringProprerty(sourceProperty, "sid", source);
				updateFieldLongProprerty(sourceProperty, "created", source);
				updateFieldLongProprerty(sourceProperty, "updated", source);
				updateFieldStringProprerty(sourceProperty, "name", source);
				updateFieldStringProprerty(sourceProperty, "type", source);
				updateFieldBooleanProprerty(sourceProperty, "space", source);
				updateFieldStringProprerty(sourceProperty, "url", source);
				updateFieldStringProprerty(sourceProperty, "sources", source);
				updateFieldStringProprerty(sourceProperty, "size", source);
			}

			JSONArray markers = null;
			try {
				markers = item.getJSONArray("marker");
			} catch (JSONException e) {
				JsonParsingException ex = new JsonParsingException("Error on parsing markers in input");
				//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				throw ex;
			}
			// delete all items in marker
			itemProperty.setValue("marker", null);
			for (int i = 0; i < markers.length(); i++) {
				try {
					itemProperty.get("marker").addValue(i, markers.get(i).toString());
				} catch (PropertyNotFoundException e) {
					JsonParsingException ex = new JsonParsingException("Error on parsing marker Item in input");
					//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					throw ex;
				} catch (PropertyException e) {
					JsonParsingException ex = new JsonParsingException("Error on parsing marker Item in input");
					//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					throw ex;
				} catch (JSONException e) {
					JsonParsingException ex = new JsonParsingException("Error on parsing marker Item in input");
					//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					throw ex;
				}
			}

			updateFieldStringProprerty(itemProperty, "color", item);
			updateFieldBooleanProprerty(itemProperty, "brand", item);
			updateFieldBooleanProprerty(itemProperty, "public", item);
			updateFieldStringProprerty(itemProperty, "css", item);
			
			itemProperty.iterator().forEachRemaining(c->log.debug(c.getName()+" "+c.getValue()));
			
			log.info("Property to update: "+itemProperty.toString());
	}

	/**
	 * Get the position of the pid item element within the document
	 * 
	 * @param pid
	 *            the IEVC project identifier
	 * @param doc
	 *            the document to update
	 * @return -1 if the pid item is not present, otherwise return the index of
	 *         Item corresponding to the input pid
	 */
	private int getPidIndex(String pid, DocumentModel doc) {
		log.info("Item size: "+doc.getProperty("evc:enrichedItems").get("Items").size());
		if (doc.getProperty("evc:enrichedItems").get("Items").size() == 0) { // items
																				// array
																				// is
																				// empty
			return -1;
		} else {
			String pidFound=(String) (doc
					.getProperty("evc:enrichedItems").get("Items").get(0).get("pid").getValue());
			log.debug("Pid found on Items of 1 length: "+pidFound);
			if (doc.getProperty("evc:enrichedItems").get("Items").size() == 1 && !pidFound.equals(pid)) { // only
																											// 1
																											// item
																											// to
																											// the
																											// array
																											// with
																											// different
																											// pid
				return -1;
			} else {
				Iterator<Property> iterator = (Iterator<Property>) doc.getProperty("evc:enrichedItems").get("Items")
						.iterator();
				int i = 0;
				while (iterator.hasNext()) {
					Property p = iterator.next();
					log.debug("Pid found: "+p.get("pid").getValue());
					if (p.get("pid").getValue().equals(pid)) {
						return i;
					}
					++i;
				}
			}
		}
		return -1;
	}

	private void updateFieldStringProprerty(Property p, String fieldName, JSONObject item)
			throws JsonParsingException, MissingFieldException {
		if (item.has(fieldName)) {
			String fieldValue = null;
			try {
				fieldValue = item.getString(fieldName);
			} catch (JSONException e) {
				JsonParsingException ex = new JsonParsingException("Error on parsing" + fieldName + "  in input");
				//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				throw ex;
			}
			p.setValue(fieldName, fieldValue);
		} else {
			MissingFieldException ex = new MissingFieldException(
					"Missing " + fieldName + " field on item body request");
			//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw ex;
		}
	}

	private void updateFieldBooleanProprerty(Property p, String fieldName, JSONObject item)
			throws JsonParsingException, MissingFieldException {
		if (item.has(fieldName)) {
			Boolean fieldValue = null;
			try {
				fieldValue = item.getBoolean(fieldName);
			} catch (JSONException e) {
				JsonParsingException ex = new JsonParsingException("Error on parsing item" + fieldName + " in input");
				//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				throw ex;
			}
			p.setValue(fieldName, fieldValue);
		} else {
			MissingFieldException ex = new MissingFieldException(
					"Missing " + fieldName + " field on item body request");
			//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw ex;
		}
	}

	private void updateFieldLongProprerty(Property p, String fieldName, JSONObject item)
			throws JsonParsingException, MissingFieldException {
		if (item.has(fieldName)) {
			Long fieldValue = null;
			try {
				fieldValue = item.getLong(fieldName);
			} catch (JSONException e) {
				JsonParsingException ex = new JsonParsingException("Error on parsing item" + fieldName + "  in input");
				//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				throw ex;
			}
			p.setValue(fieldName, fieldValue);
		} else {
			MissingFieldException ex = new MissingFieldException(
					"Missing " + fieldName + " field on item body request");
			//ex.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			throw ex;
		}
	}

}
