package eu.project.producer.mcssr;

//Author: Giuseppe Filomena
//Date: 27-02-2018

//package org.nuxeo.ecm.platform.video.tools.operations;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.IdRef;
import org.nuxeo.ecm.core.api.PathRef;
import org.nuxeo.ecm.core.work.api.Work;
import org.nuxeo.ecm.core.work.api.WorkManager;
import org.nuxeo.ecm.core.work.api.Work.State;
import org.nuxeo.ecm.platform.video.service.VideoConversionWork;
import org.nuxeo.ecm.platform.video.service.VideoService;
import org.nuxeo.runtime.api.Framework;


/**
*
*/
@Operation(id=VideoConverterService.ID, category=Constants.CAT_DOCUMENT, label="VideoConverterService", description="Launch the Video conversion with an another videotype as 'MP4 480p', 'WebM 480p', 'Ogg 480p'.   Insert the docId or path.")
public class VideoConverterService {

  public static final String ID = "Document.VideoConverterService";
  
  private static final Log log = LogFactory.getLog(VideoConverterService.class);
  
  @Context
  protected CoreSession session;
  
	@Param(name = "docId", required = false)
	protected String docId;
	
	@Param(name = "path", required = false)
	protected String path;
  
  @Param(name = "videotype", required = false)
  protected String videotype ;

	
  
  @OperationMethod
  public String run() throws InterruptedException, JSONException {
  	if(videotype == null) {
  		videotype  = "WebM 480p";
  	}
  	VideoService videoService = Framework.getService(VideoService.class);

  	
		DocumentModel doc = null;
		if (StringUtils.isBlank(path) && StringUtils.isBlank(docId)) {
			return "The parameters path and docId are empty! One of them is required.";
		} 
		if (!StringUtils.isBlank(docId)){
			doc = session.getDocument(new IdRef(docId));
		}else{
			doc = session.getDocument(new PathRef(path));
		}
		
		log.info("ConverterService start");

		@SuppressWarnings("unchecked")
		List<Map<String, Serializable>> transcodedVideos = (List<Map<String, Serializable>>) doc.getPropertyValue("vid:transcodedVideos");
		
		if(!isConverted(transcodedVideos, videotype)) {
			
			
			  
			State status = GetVideoConversionStatus(doc.getRepositoryName(), doc.getId(), videotype);

			if(status == null) {
				
				videoService.launchConversion(doc, videotype);
				
				return "STARTED";
				
		      /*  while (videoService.getProgressStatus(doc.getRepositoryName(), doc.getId(), videotype) != null) {
		            // wait for the conversion to complete
		            Thread.sleep(2000);
		        }
		      */
		        
		        
			}else{
				 return status.name();
			}
	
				
				
			
		}else{
			session.save();
			return videotype+" :CONVERTED";
		}
		

		/* 
		
		VideoConversionStatus status = null;
		status = videoService.getProgressStatus(doc.getRepositoryName(), doc.getId(), videotype);
		
      while (videoService.getProgressStatus(doc.getRepositoryName(), doc.getId(), videotype) != null) {
          // wait for the conversion to complete
          Thread.sleep(2000);
      }
      

		if(status != null)  {
			log.info("before message:"+status.message);
			log.info("before positionInQueue:"+status.positionInQueue);
			log.info("before queueSize:"+status.queueSize);
			session.save();
			return "The conversion process is already running - message:"+status.message+ " - positionInQueue:"+status.positionInQueue+" -  queueSize:"+status.queueSize;
		}else{

			if(isConverted(transcodedVideos, videotype)) {
				return "The video was already converted with the format:"+videotype;
			}else{
		        videoService.launchConversion(doc, videotype);
		        
		        while (videoService.getProgressStatus(doc.getRepositoryName(), doc.getId(), videotype) != null) {
		            // wait for the conversion to complete
		            Thread.sleep(2000);
		        }
				session.save();
				return "The Video conversion has been launched!";
			}
		}
		
		*/
		
  }
  
  private State GetVideoConversionStatus(String repositoryName, String docId, String conversionName) {
          WorkManager workManager = Framework.getLocalService(WorkManager.class);
          Work work = new VideoConversionWork(repositoryName, docId, conversionName);
          State state = workManager.getWorkState(work.getId());
  	
		return state;
  }
  
  
  
  private boolean isConverted(List<Map<String, Serializable>> transcodedVideos, String videotype) {
  	
  	Boolean result = false;
  	for(int i = 0; i < transcodedVideos.size(); i++) {
			
  		log.info(transcodedVideos.get(i).get("name").toString()+" =  "+ videotype + " ->" +transcodedVideos.get(i).get("name").toString().equals(videotype));
  		
			if(transcodedVideos.get(i).get("name").toString().equals(videotype)) {
				result = true;
			}
		}
		return result; 	
  }
}
