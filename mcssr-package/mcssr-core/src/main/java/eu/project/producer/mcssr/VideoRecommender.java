
package eu.project.producer.mcssr;

import java.io.IOException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.codehaus.jackson.JsonGenerator;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.Constants;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.core.annotations.Operation;
import org.nuxeo.ecm.automation.core.annotations.OperationMethod;
import org.nuxeo.ecm.automation.core.annotations.Param;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.IdRef;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.transaction.TransactionHelper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import eu.project.producer.mcssr.exception.VideoRecommenderException;
import eu.project.producer.mcssr.service.DocumentService;
import eu.project.producer.mcssr.service.SrptService;
import net.sf.json.JSONObject;


/**
 *
 */
@Operation(id=VideoRecommender.ID, category=Constants.CAT_DOCUMENT, label="VideoRecommender", description="Describe here what your operation does.")
public class VideoRecommender {

    public static final String ID = "Document.VideoRecommender";
    
    private static final Log log = LogFactory.getLog(VideoRecommender.class);
    
    private static final String ievcEndpoint= Framework.getProperty("ievc.play.video.service");

    @Context
    protected CoreSession session;
    
    @Context
    private DocumentService documentService=Framework.getService(DocumentService.class);


    @Param(name = "uid", required = true)
    protected String uid;

    @Param(name = "username", required = true)
    protected String username;

    @OperationMethod
    public String run() throws OperationException, IOException {
    
		DocumentModel doc;

		IdRef idRef = new IdRef(uid);
		doc = session.getDocument(idRef);

		int num = doc.getProperty("evc:enrichedItems").get("Items").size();
		log.debug("Items size:"+num);    
		
		
        if(num == 1) {
        	
        	String pid=(String)doc.getProperty("evc:enrichedItems/Items/0/pid").getValue();
        	String url=ievcEndpoint+pid;
        	return url;
        	
        }else if(num > 1){
        	
        	JsonNode itemsJsonNode=documentService.getJsonOfProperty(doc.getProperty("evc:enrichedItems/Items"));
			JsonNode unescapedItemsJsonNode=documentService.convertItemsArrayWithUnescapedMarkers(itemsJsonNode);
        	
			SrptService srptService=Framework.getService(SrptService.class);
        	JsonNode response = null;
			try {
				response = srptService.getRecommendedVideo(username, unescapedItemsJsonNode);
			} catch (UnsupportedOperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new VideoRecommenderException("Error during the invocation of SRPT Service!");
			}catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new VideoRecommenderException("Error during the invocation of SRPT Service!");
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new VideoRecommenderException("Error during the invocation of SRPT Service!");
			}

            return response.get("URL").asText();
        }
		return "No Items found on document";
    }
    

}
