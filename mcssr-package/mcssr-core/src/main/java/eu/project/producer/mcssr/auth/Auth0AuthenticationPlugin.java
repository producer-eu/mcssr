package eu.project.producer.mcssr.auth;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.ecm.core.api.NuxeoException;
import org.nuxeo.ecm.platform.api.login.UserIdentificationInfo;
import org.nuxeo.ecm.platform.oauth2.openid.OpenIDConnectProvider;
import org.nuxeo.ecm.platform.oauth2.openid.OpenIDConnectProviderRegistry;
import org.nuxeo.ecm.platform.oauth2.openid.auth.OpenIDUserInfo;
import org.nuxeo.ecm.platform.oauth2.openid.auth.UserResolver;
import org.nuxeo.ecm.platform.ui.web.auth.interfaces.NuxeoAuthenticationPlugin;
import org.nuxeo.ecm.platform.ui.web.auth.interfaces.NuxeoAuthenticationPluginLogoutExtension;
import org.nuxeo.ecm.platform.usermanager.UserManager;
import org.nuxeo.runtime.api.Framework;

import com.auth0.AuthenticationController;

import static org.nuxeo.ecm.platform.ui.web.auth.NXAuthConstants.LOGIN_ERROR;

public class Auth0AuthenticationPlugin implements NuxeoAuthenticationPlugin, NuxeoAuthenticationPluginLogoutExtension {

	private static final Log log = LogFactory.getLog(Auth0AuthenticationPlugin.class);

	public static final String STATE_URL_PARAM_NAME = "state";

	public static final String STATE_SESSION_ATTRIBUTE = STATE_URL_PARAM_NAME;

	public static final String CODE_URL_PARAM_NAME = "code";

	public static final String ERROR_URL_PARAM_NAME = "error";

	public static final String PROVIDER_URL_PARAM_NAME = "provider";

	public static final String USERINFO_KEY = "OPENID_USERINFO";

	public static final String PROPERTY_OAUTH_CREATE_USER = "nuxeo.oauth.auth.create.user";

	public static final String PROPERTY_SKIP_OAUTH_TOKEN = "nuxeo.skip.oauth.token.state.check";
	
	private final String AUTH0_DOMAIN=Framework.getProperty("auth0.domain");
	
	private final String AUTH0_CLIENT_ID=Framework.getProperty("auth0.clientId");
	
	private final String AUTH0_CLIENT_SECRET=Framework.getProperty("auth0.clientSecret");

	protected void sendError(HttpServletRequest req, String msg) {
		req.setAttribute(LOGIN_ERROR, msg);
	}

	public UserIdentificationInfo retrieveIdentityFromOAuth(HttpServletRequest req, HttpServletResponse resp) {

		// Getting the "error" URL parameter
		String error = req.getParameter(ERROR_URL_PARAM_NAME);

		// / Checking if there was an error such as the user denied access
		if (error != null && error.length() > 0) {
			sendError(req, "There was an error: \"" + error + "\".");
			String logoutUrl=String.format("https://%s/v2/logout?client_id=%s&returnTo=http://producer-toolkit.eu/", AUTH0_DOMAIN,AUTH0_CLIENT_ID);
		    //req.getSession().invalidate();
			try {
				resp.sendRedirect(logoutUrl);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		// Getting the "code" URL parameter
		String code = req.getParameter(CODE_URL_PARAM_NAME);

		// Checking conditions on the "code" URL parameter
		if (code == null || code.isEmpty()) {
			sendError(req, "There was an error: \"" + code + "\".");
			return null;
		}

		// Getting the "provider" URL parameter
		String serviceProviderName = req.getParameter(PROVIDER_URL_PARAM_NAME);

		// Checking conditions on the "provider" URL parameter
		if (serviceProviderName == null || serviceProviderName.isEmpty()) {
			sendError(req, "Missing OpenID Connect Provider ID.");
			return null;
		}

		try {
			OpenIDConnectProviderRegistry registry = Framework.getLocalService(OpenIDConnectProviderRegistry.class);
			OpenIDConnectProvider provider = registry.getProvider(serviceProviderName);

			if (provider == null) {
				sendError(req, "No service provider called: \"" + serviceProviderName + "\".");
				return null;
			}

			// Check the state token

			if (!Framework.isBooleanPropertyTrue(PROPERTY_SKIP_OAUTH_TOKEN) && !provider.verifyStateToken(req)) {
				sendError(req, "Invalid state parameter.");
			}

			// Validate the token
			String accessToken = provider.getAccessToken(req, code);

			if (accessToken == null) {
				return null;
			}

			OpenIDUserInfo info = provider.getUserInfo(accessToken);

			// Store the user info as a key in the request so apps can use it
			// later in the chain
			req.setAttribute(USERINFO_KEY, info);

			UserResolver userResolver = provider.getUserResolver();

			String userId;
			if (Framework.isBooleanPropertyTrue(PROPERTY_OAUTH_CREATE_USER)) {
				userId = userResolver.findOrCreateNuxeoUser(info);
			} else {
				userId = findNuxeoUser(info);
			}

			if (userId == null) {

				sendError(req, "No user found with email: \"" + info.getEmail() + "\".");
				return null;
			}

			return new UserIdentificationInfo(userId, userId);

		} catch (NuxeoException e) {
			log.error("Error while retrieve Identity From OAuth", e);
		}

		return null;
	}

	private String findNuxeoUser(OpenIDUserInfo userInfo) {

		try {
			UserManager userManager = Framework.getLocalService(UserManager.class);
			Map<String, Serializable> query = new HashMap<String, Serializable>();
			query.put(userManager.getUserEmailField(), userInfo.getEmail());

			DocumentModelList users = userManager.searchUsers(query, null);

			if (users.isEmpty()) {
				return null;
			}

			DocumentModel user = users.get(0);
			return (String) user.getPropertyValue(userManager.getUserIdField());

		} catch (NuxeoException e) {
			log.error("Error while search user in UserManager using email " + userInfo.getEmail(), e);
			return null;
		}
	}

	@Override
	public List<String> getUnAuthenticatedURLPrefix() {
		return new ArrayList<String>();
	}

	@Override
	public UserIdentificationInfo handleRetrieveIdentity(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse) {

		String error = httpRequest.getParameter(ERROR_URL_PARAM_NAME);
		String code = httpRequest.getParameter(CODE_URL_PARAM_NAME);
		String serviceProviderName = httpRequest.getParameter(PROVIDER_URL_PARAM_NAME);
		if (serviceProviderName == null && code == null && error == null) {
			checkSession(httpRequest, httpResponse);
			return null;
		}
		UserIdentificationInfo userIdent = retrieveIdentityFromOAuth(httpRequest, httpResponse);
		if (userIdent != null) {
			userIdent.setAuthPluginName("TRUSTED_LM");
		}
		return userIdent;
	}

	private void checkSession(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
		AuthenticationController authController;
		authController = AuthenticationController.newBuilder(AUTH0_DOMAIN, AUTH0_CLIENT_ID, AUTH0_CLIENT_SECRET).build();

		String redirectUri;
		if(httpRequest.getServerName().contains("localhost")){
			redirectUri = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":"
				+ httpRequest.getServerPort() + "/nuxeo/ui/?provider=Auth0OpenIDConnect&forceAnonymousLogin=true";
		}else{
			redirectUri = httpRequest.getScheme() + "://" + httpRequest.getServerName() + "/nuxeo/ui/?provider=Auth0OpenIDConnect&forceAnonymousLogin=true";
		}
		String authorizeUrl = authController.buildAuthorizeUrl(httpRequest, redirectUri)
				.withAudience(String.format("https://%s/userinfo", AUTH0_DOMAIN))
				.withScope("openid name email profile")
				.withParameter("prompt", "none").build();
		try {
			httpResponse.sendRedirect(authorizeUrl);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Boolean handleLoginPrompt(HttpServletRequest httpRequest, HttpServletResponse httpResponse, String baseURL) {
		return false;
	}

	@Override
	public Boolean needLoginPrompt(HttpServletRequest httpRequest) {
		return false;
	}

	@Override
	public void initPlugin(Map<String, String> parameters) {
	}

	@Override
	public Boolean handleLogout(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
		// TODO Auto-generated method stub
		String logoutUrl=String.format("https://%s/v2/logout?client_id=%s&returnTo=http://producer-toolkit.eu/", AUTH0_DOMAIN,AUTH0_CLIENT_ID);
	    //req.getSession().invalidate();
		try {
			httpResponse.sendRedirect(logoutUrl);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    return true;
	}
}
