/*
 * Author: Giuseppe Filomena
 */
package eu.project.producer.mcssr.auth;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.api.client.util.Key;

public class User_Metadata {

	@Key("user")
	protected String user;

	public JSONObject getUser() {
		JSONObject jsonUser;
		try {
			jsonUser = new JSONObject(user);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonUser;
	}
	
}
