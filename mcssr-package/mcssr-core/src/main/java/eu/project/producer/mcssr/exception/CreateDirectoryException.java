package eu.project.producer.mcssr.exception;

import org.nuxeo.ecm.automation.server.jaxrs.RestOperationException;

public class CreateDirectoryException extends RestOperationException {

    private static final long serialVersionUID = 7123858603327032114L;

    public CreateDirectoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreateDirectoryException(String message) {
        super(message);
    }

    public CreateDirectoryException(Throwable cause) {
        super(cause);
    }

}
