package eu.project.producer.mcssr.exception;

import org.nuxeo.ecm.automation.server.jaxrs.RestOperationException;

public class FileDeletionException extends RestOperationException {

    private static final long serialVersionUID = 7123858603327032114L;

    public FileDeletionException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileDeletionException(String message) {
        super(message);
    }

    public FileDeletionException(Throwable cause) {
        super(cause);
    }

}
