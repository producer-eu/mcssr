package eu.project.producer.mcssr.exception;

import org.nuxeo.ecm.automation.server.jaxrs.RestOperationException;

public class HttpClientException extends RestOperationException {

    private static final long serialVersionUID = 7123858603327032114L;

    public HttpClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpClientException(String message) {
        super(message);
    }

    public HttpClientException(Throwable cause) {
        super(cause);
    }

}