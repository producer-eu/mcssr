package eu.project.producer.mcssr.exception;

import org.nuxeo.ecm.automation.server.jaxrs.RestOperationException;

public class JsonParsingException extends RestOperationException {

    private static final long serialVersionUID = 7123858603327032114L;

    public JsonParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonParsingException(String message) {
        super(message);
    }

    public JsonParsingException(Throwable cause) {
        super(cause);
    }
}