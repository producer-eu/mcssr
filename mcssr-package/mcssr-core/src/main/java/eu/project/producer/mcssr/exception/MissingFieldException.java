package eu.project.producer.mcssr.exception;

import org.nuxeo.ecm.automation.server.jaxrs.RestOperationException;

public class MissingFieldException extends RestOperationException {

    private static final long serialVersionUID = 7123858603327032114L;

    public MissingFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingFieldException(String message) {
        super(message);
    }

    public MissingFieldException(Throwable cause) {
        super(cause);
    }
}