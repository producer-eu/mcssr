package eu.project.producer.mcssr.exception;

import org.nuxeo.ecm.automation.server.jaxrs.RestOperationException;

public class SemanticSearchException extends RestOperationException {

    public SemanticSearchException(String message) {
        super(message);
    }

}
