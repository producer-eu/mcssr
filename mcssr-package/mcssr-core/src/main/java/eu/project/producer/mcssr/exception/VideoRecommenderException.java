package eu.project.producer.mcssr.exception;

import org.nuxeo.ecm.automation.server.jaxrs.RestOperationException;

public class VideoRecommenderException extends RestOperationException{
	
	private static final long serialVersionUID = 1L;

	public VideoRecommenderException(String message, Throwable cause) {
        super(message, cause);
    }

    public VideoRecommenderException(String message) {
        super(message);
    }

    public VideoRecommenderException(Throwable cause) {
        super(cause);
    }

}
