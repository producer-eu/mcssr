package eu.project.producer.mcssr.exception;

import org.nuxeo.ecm.core.api.NuxeoException;

public class Vpt360Exception extends NuxeoException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    public Vpt360Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public Vpt360Exception(String message) {
        super(message);
    }

    public Vpt360Exception(Throwable cause) {
        super(cause);
    }
}
