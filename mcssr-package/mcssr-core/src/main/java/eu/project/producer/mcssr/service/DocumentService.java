package eu.project.producer.mcssr.service;

import java.net.URL;

import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.model.Property;
import org.nuxeo.ecm.core.event.EventContext;

import com.fasterxml.jackson.databind.JsonNode;

public interface DocumentService {
    /** Add some methods here. **/
	
	JsonNode getJsonOfProperty(Property p);
	
	JsonNode getJsonAnnotation(DocumentModel doc);
	
	void updateDocProperty(DocumentModel doc, String xpath, String value);
	
	JsonNode convertItemsArrayWithUnescapedMarkers(JsonNode itemsJsonNode);

	URL getPreSignedAmazonURL(String digest);
	
	void downloadBlobFromUrl(DocumentModel doc, EventContext ctx);

	boolean isUploadedFromOcd(DocumentModel doc);

	boolean isProperlyFilledContent(DocumentModel doc);

	boolean isMultimediaContent(DocumentModel doc);

	boolean is360Video(DocumentModel doc);
	
}
