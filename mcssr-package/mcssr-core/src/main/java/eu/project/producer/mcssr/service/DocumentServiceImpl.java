package eu.project.producer.mcssr.service;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationContext;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.util.DocumentHelper;
import org.nuxeo.ecm.core.api.Blob;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.PropertyException;
import org.nuxeo.ecm.core.api.model.Property;
import org.nuxeo.ecm.core.api.model.impl.ListProperty;
import org.nuxeo.ecm.core.api.model.impl.MapProperty;
import org.nuxeo.ecm.core.api.model.impl.ScalarProperty;
import org.nuxeo.ecm.core.api.model.impl.primitives.BlobProperty;
import org.nuxeo.ecm.core.event.EventContext;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.model.ComponentContext;
import org.nuxeo.runtime.model.ComponentInstance;
import org.nuxeo.runtime.model.DefaultComponent;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class DocumentServiceImpl extends DefaultComponent implements DocumentService {

	private final Log log = LogFactory.getLog(DocumentServiceImpl.class);

	private DateFormat dateFormat;
	
	private  String awsKey;

	private String awsSecret;

	private String awsBucket;

	private String awsRegion;

	private String awsSignedUrlDuration;

	/**
	 * Component activated notification. Called when the component is activated.
	 * All component dependencies are resolved at that moment. Use this method
	 * to initialize the component.
	 *
	 * @param context
	 *            the component context.
	 */
	@Override
	public void activate(ComponentContext context) {
		super.activate(context);
	}

	/**
	 * Component deactivated notification. Called before a component is
	 * unregistered. Use this method to do cleanup if any and free any resources
	 * held by the component.
	 *
	 * @param context
	 *            the component context.
	 */
	@Override
	public void deactivate(ComponentContext context) {
		super.deactivate(context);
	}

	/**
	 * Application started notification. Called after the application started.
	 * You can do here any initialization that requires a working application
	 * (all resolved bundles and components are active at that moment)
	 *
	 * @param context
	 *            the component context. Use it to get the current bundle
	 *            context
	 * @throws Exception
	 */
	@Override
	public void applicationStarted(ComponentContext context) {
		// do nothing by default. You can remove this method if not used.
		dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZ");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		awsKey = Framework.getProperty("nuxeo.s3storage.awsid");

		awsSecret = Framework.getProperty("nuxeo.s3storage.awssecret");

		awsBucket = Framework.getProperty("nuxeo.s3storage.bucket");

		awsRegion = Framework.getProperty("nuxeo.s3storage.region");

		awsSignedUrlDuration = Framework.getProperty("nuxeo.s3storage.signedurl.duration");
	}

	@Override
	public void registerContribution(Object contribution, String extensionPoint, ComponentInstance contributor) {
		// Add some logic here to handle contributions
	}

	@Override
	public void unregisterContribution(Object contribution, String extensionPoint, ComponentInstance contributor) {
		// Logic to do when unregistering any contribution
	}

	@Override
	public JsonNode getJsonAnnotation(DocumentModel doc) {
		// Property ann = doc.getProperty("ann:Annotation");

		JsonNode jsonNode = getJsonOfProperty(doc.getProperty("ann:Annotation"));

		log.debug("Json Node: " + jsonNode.textValue());
		log.debug("ANNOTATION NODE" + doc.getPropertyValue("ann:Annotation").toString());
		JSONObject jsonAnnotation = null;
		final String inputId = doc.getId();
		ObjectMapper mapper = new ObjectMapper();

		try {
			jsonAnnotation = new JSONObject(doc.getPropertyValue("ann:Annotation").toString());
			log.debug("jsonAnnotation:" + jsonAnnotation.toString());
		} catch (PropertyException e1) {
			// TODO Auto-generated catch block
			log.debug("PropertyException e1" + e1.toString());
			e1.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			log.debug("JSONException e1" + e1.toString());
			e1.printStackTrace();
		}
		JsonNode jNode = null;
		try {
			jNode = mapper.readTree(jsonAnnotation.toString());
			// jNode = mapper.readValue(jsonAnnotation.toString(),
			// JsonNode.class);
			log.debug("jNode:" + jNode.toString());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("ANNOTATION NODE" + jsonAnnotation.toString());
		return jNode;
	}

	@Override
	public void updateDocProperty(DocumentModel doc, String xpath, String value) {
		try {
			DocumentHelper.setProperty(doc.getCoreSession(), doc, xpath, value);
		} catch (IOException e) {
			log.error("Error on update document property!");
			e.printStackTrace();
		}
	}

	@Override
	public JsonNode getJsonOfProperty(Property p) {
		com.fasterxml.jackson.core.JsonFactory factory = new com.fasterxml.jackson.core.JsonFactory();
		Writer stringWriter = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		JsonGenerator jg = null;
		JsonNode jsonProperty = null;
		try {
			jg = factory.createGenerator(stringWriter);
			jg.writeStartObject();
			if (p instanceof MapProperty)
				writeJsonOfMapProperty((MapProperty) p, jg);
			if (p instanceof ListProperty)
				writeJsonOfListProperty((ListProperty) p, jg);
			if (p instanceof ScalarProperty)
				writeJsonOfScalarProperty((ScalarProperty) p, jg);
			jg.writeEndObject();
			jg.close();
			jsonProperty = mapper.readTree(stringWriter.toString());
			log.debug("Json Node prior of jg close: " + jsonProperty.toString());
		} catch (IOException e) {
			log.error(e);
			e.printStackTrace();
		}
		return jsonProperty;
	}

	private void writeJsonOfMapProperty(MapProperty mapProperty, JsonGenerator jg) throws IOException {
		if (mapProperty instanceof BlobProperty) {
			log.warn("Property '" + mapProperty.getName()
					+ "' ignored during serialization. Blob and blob related properties are not written to json object.");
			return;
		} else if (mapProperty.getParent() instanceof BlobProperty) {
			log.warn("Property '" + mapProperty.getName()
					+ "' ignored during serialization. Blob and blob related properties are not written to json object.");
			return;
		} else if (mapProperty.getParent().isList()) {
			jg.writeStartObject();
		} else {
			jg.writeObjectFieldStart(mapProperty.getField().getName().getPrefixedName());
		}
		for (Property child : mapProperty.getChildren()) {
			if (child instanceof MapProperty) {
				writeJsonOfMapProperty((MapProperty) child, jg);
			}
			if (child instanceof ListProperty) {
				writeJsonOfListProperty((ListProperty) child, jg);
			}
			if (child instanceof ScalarProperty) {
				writeJsonOfScalarProperty((ScalarProperty) child, jg);
			}
		}
		// end of Map Property, close json object
		jg.writeEndObject();

	}

	private void writeJsonOfListProperty(ListProperty listProperty, JsonGenerator jg) throws IOException {
		if (listProperty.getParent() instanceof BlobProperty) {
			log.warn("Property '" + listProperty.getName()
					+ "' ignored during serialization. Blob and blob related properties are not written to json object.");
			return;
		}
		jg.writeArrayFieldStart(listProperty.getField().getName().getPrefixedName());
		for (Property child : listProperty.getChildren()) {
			if (child instanceof MapProperty) {
				writeJsonOfMapProperty((MapProperty) child, jg);
			}
			if (child instanceof ListProperty) {
				writeJsonOfListProperty((ListProperty) child, jg);
			}
			if (child instanceof ScalarProperty) {
				writeJsonOfScalarProperty((ScalarProperty) child, jg);
			}
		}
		// end of List Property, close json array
		jg.writeEndArray();
	}

	private void writeJsonOfScalarProperty(ScalarProperty scalarProperty, JsonGenerator jg) throws IOException {
		if (scalarProperty.getParent() instanceof BlobProperty) {
			log.warn("Property '" + scalarProperty.getName()
					+ "' ignored during serialization. Blob and blob related properties are not written to json object.");
			return;
		}
		Serializable value = scalarProperty.getValue();
		if (!scalarProperty.getParent().isList()) {
			jg.writeFieldName(scalarProperty.getField().getName().getPrefixedName());
		}
		if (value == null) {
			jg.writeNull();
			return;
		}
		if (value instanceof Calendar) {
			value = dateFormat.format(((Calendar) value).getTime());
		}
		if (value instanceof Double) {
			jg.writeNumber((Double) value);
		} else if (value instanceof Integer) {
			jg.writeNumber((Integer) value);
		} else if (value instanceof Long) {
			jg.writeNumber((Long) value);
		} else if (value instanceof Boolean) {
			jg.writeBoolean((Boolean) value);
		} else if (value instanceof String) {
			jg.writeString((String) value);
		}
		return;
	}

	@Override
	public JsonNode convertItemsArrayWithUnescapedMarkers(JsonNode itemsJsonNode) {
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode itemsArray = (ArrayNode) itemsJsonNode.get("Items");
		for (JsonNode itemsArrayElement : itemsArray) {
			ArrayNode unescapedMarkersArray = mapper.createArrayNode();
			ArrayNode markersArray = (ArrayNode) itemsArrayElement.get("marker");
			for (JsonNode markersArrayElement : markersArray) {
				String opt = StringEscapeUtils.unescapeJson(markersArrayElement.toString());
				opt = opt.substring(1, opt.length() - 1);
				JsonNode unescapedMarkersArrayElement = null;
				try {
					unescapedMarkersArrayElement = mapper.readTree(opt);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				unescapedMarkersArray.add(unescapedMarkersArrayElement);
			}
			//((ObjectNode) itemsArrayElement).remove("marker");
			((ObjectNode) itemsArrayElement).replace("marker", unescapedMarkersArray);
		}
		log.debug("UNESCAPED ITEM NODE: " + itemsJsonNode.toString());
		return itemsJsonNode;
	}
	
	@Override
	public URL getPreSignedAmazonURL(String digest ) {
		URL signedURL = null;
		// String objectKey = (String)
		// doc.getProperty("file:content").get("digest").getValue();
		log.debug("Object Key to retrieve: " + digest);
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsKey, awsSecret);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCreds))
				.withRegion(Regions.fromName(awsRegion)).build();

		java.util.Date expiration = new java.util.Date();
		long msec = expiration.getTime();
		msec += 1000 * 60 * Long.valueOf(awsSignedUrlDuration);
		expiration.setTime(msec);

		GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(awsBucket, digest);
		generatePresignedUrlRequest.setMethod(HttpMethod.GET);
		generatePresignedUrlRequest.setExpiration(expiration);

		signedURL = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
		log.debug("Signed Amazon URL: " + signedURL);
		return signedURL;
	}
	
	@Override
	public void downloadBlobFromUrl(DocumentModel doc, EventContext ctx) {
		if (!isProperlyFilledContent(doc) || !isUploadedFromOcd(doc)) {
			log.info("Not a Document that requires download of Blob content! Skip processing");
			return;
		}
		log.info("Downloading multimedia content for Document: " + doc.getPropertyValue("dc:title"));
		// Add some logic starting from here.
		AutomationService automationService = Framework.getService(AutomationService.class);
		Map<String, Object> params = new HashMap<>();
		OperationContext coop = new OperationContext(ctx.getCoreSession());
		params.put("file", doc.getPropertyValue("mul:url"));
		params.put("encoding", doc.getPropertyValue("mul:encoding"));
		params.put("filename", doc.getPropertyValue("dc:title"));
		params.put("mime-type", doc.getPropertyValue("mul:mime-type"));

		Blob blob = null;
		try {
			blob = (Blob) automationService.run(coop, "Blob.CreateFromURL", params);
		} catch (OperationException e) {
			log.error("Error during the creation of Blob " + doc.getPropertyValue("dc:title") + " from URL!");
			log.error(e.getMessage());
		}
		log.debug("Blob: " + blob.getFilename() + " created!");
		coop = new OperationContext(ctx.getCoreSession());
		coop.setInput(blob);
		params = new HashMap<>();
		params.put("document", doc);

		DocumentHelper.addBlob(doc.getProperty("file:content"), blob);

		log.info("Document " + doc.getPropertyValue("dc:title") + " updated with content and saved!");

	}
	
	@Override
	public boolean isUploadedFromOcd(DocumentModel doc) {
		if (doc.getPropertyValue("dc:source") != null && !doc.getPropertyValue("dc:source").equals("Producer"))
			return true;
		return false;
	}

	@Override
	public boolean isProperlyFilledContent(DocumentModel doc) {
		if (isMultimediaContent(doc))
			if (doc.getPropertyValue("mul:url") != null && doc.getPropertyValue("mul:encoding") != null
					&& doc.getPropertyValue("dc:title") != null && doc.getPropertyValue("mul:mime-type") != null)
				return true;
		return false;
	}

	@Override
	public boolean isMultimediaContent(DocumentModel doc) {
		return doc.getType().equals("Video") || doc.getType().equals("Audio") || doc.getType().equals("Picture")
				|| doc.getType().equals("File");
	}
	
	@Override
	public boolean is360Video(DocumentModel doc){
		return (boolean) doc.getPropertyValue("mul:video360");
	}
}
