package eu.project.producer.mcssr.service;

import java.util.Map;

import org.nuxeo.ecm.core.api.DocumentModel;

import com.fasterxml.jackson.databind.JsonNode;

public interface McssrTagService {
    /** Add some methods here. **/
	
	void convertAnnotationToTagOnDocument(DocumentModel doc);
	
	void updateTagsAndTimecodes(DocumentModel doc, Map<String, String> mapOfTimecodesByTag);
	
	void createTags(DocumentModel doc, Map<String, String> mapOfTags);
	
	Map<String, String> getMapOfTimecodesByTag(JsonNode jsonAnnotation);
}
