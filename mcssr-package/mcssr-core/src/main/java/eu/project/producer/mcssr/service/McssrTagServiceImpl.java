package eu.project.producer.mcssr.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.ecm.platform.tag.TagService;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.model.ComponentContext;
import org.nuxeo.runtime.model.ComponentInstance;
import org.nuxeo.runtime.model.DefaultComponent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class McssrTagServiceImpl extends DefaultComponent implements McssrTagService {
	
	private Log log = LogFactory.getLog(McssrTagService.class);

	@Context
	private DocumentService documentService;
    /**
     * Component activated notification.
     * Called when the component is activated. All component dependencies are resolved at that moment.
     * Use this method to initialize the component.
     *
     * @param context the component context.
     */
    @Override
    public void activate(ComponentContext context) {
        super.activate(context);
    }

    /**
     * Component deactivated notification.
     * Called before a component is unregistered.
     * Use this method to do cleanup if any and free any resources held by the component.
     *
     * @param context the component context.
     */
    @Override
    public void deactivate(ComponentContext context) {
        super.deactivate(context);
    }

    /**
     * Application started notification.
     * Called after the application started.
     * You can do here any initialization that requires a working application
     * (all resolved bundles and components are active at that moment)
     *
     * @param context the component context. Use it to get the current bundle context
     * @throws Exception
     */
    @Override
    public void applicationStarted(ComponentContext context) {
        // do nothing by default. You can remove this method if not used.
        documentService=Framework.getService(DocumentService.class);
    }

    @Override
    public void registerContribution(Object contribution, String extensionPoint, ComponentInstance contributor) {
        // Add some logic here to handle contributions
    }

    @Override
    public void unregisterContribution(Object contribution, String extensionPoint, ComponentInstance contributor) {
        // Logic to do when unregistering any contribution
    }
    
    public void convertAnnotationToTagOnDocument(DocumentModel doc) {
		updateTagsAndTimecodes(doc, getMapOfTimecodesByTag(documentService.getJsonOfProperty(doc.getProperty("ann:Annotation"))));
	}

	public void updateTagsAndTimecodes(DocumentModel doc, Map<String, String> mapOfTimecodesByTag) {
		createTags(doc, mapOfTimecodesByTag);
		createTimecodes(doc, mapOfTimecodesByTag);
	}

	public void createTags(DocumentModel doc, Map<String, String> mapOfTags) {
		createTagsFromList(doc, mapOfTags.keySet());
	}

	private void createTagsFromList(DocumentModel doc, Set<String> tagSet) {
		tagSet.forEach(a->log.debug("Annotated Tags are: "+a));
		TagService tagService = Framework.getService(TagService.class);
		Set<String> currentTagStrings=tagService.getDocumentTags(doc.getCoreSession(), doc.getId(), null).stream()
				.map(t->t.getLabel())
				.collect(Collectors.toSet());
		currentTagStrings.forEach(a->log.debug("Current Tags of Document "+doc.getProperty("dc:title").getValue()+" are: "+a));
		
		tagSet.stream()
		.filter(a->!currentTagStrings.contains(a))
		.forEach(t->{
			log.debug("Tagging annotation: "+t);
			tagService.tag(doc.getCoreSession(), doc.getId(), t, "system");
		});
		
		tagService.getDocumentTags(doc.getCoreSession(), doc.getId(), null).forEach(a->log.debug("Tag of Document "+doc.getProperty("dc:title").getValue()+" are: "+a.getLabel()));
	}
	
	private void createTimecodes(DocumentModel doc, Map<String, String> mapOfTimecodesByTag) {
		CoreSession session = doc.getCoreSession();
		for (String tag : mapOfTimecodesByTag.keySet()) {
			String query = String.format("SELECT * FROM Tag WHERE tag:label = '%s'", tag);
			DocumentModelList tagDocList = (DocumentModelList) session.query(query);
			log.debug("Documetn tag list size: " + tagDocList.size());
			if(tagDocList.size()>0){
			DocumentModel tagDoc = tagDocList.get(0);
			String tagId = tagDoc.getId();
			query = String.format("SELECT * FROM Tagging WHERE relation:target='%s' AND relation:source='%s'", tagId,
					doc.getId());
			}
			DocumentModelList taggingDocList = (DocumentModelList) session.query(query);
			log.debug("Document tagging list size: " + taggingDocList.size());
			if(taggingDocList.size()>0){
			DocumentModel taggingDoc = taggingDocList.get(0);
			taggingDoc.setPropertyValue("relation:timecode", mapOfTimecodesByTag.get(tag));
			taggingDoc.getCoreSession().saveDocument(taggingDoc);
			}
		}

	}

	public Map<String, String> getMapOfTimecodesByTag(JsonNode jsonAnnotation) {
		ArrayNode objectNode = (ArrayNode) jsonAnnotation.path("ann:Annotation").path("objectdetection");
		log.debug("OBJECT NODE" + objectNode.toString());
		ArrayNode faceNode = (ArrayNode) jsonAnnotation.path("ann:Annotation").path("facedetection");
		log.debug("FACE NODE" + faceNode.toString());
		List<JsonNode> objectNodeList = IteratorUtils.toList(objectNode.iterator());
		List<JsonNode> faceNodeList = IteratorUtils.toList(faceNode.iterator());
		
		// tag with space go in error

		Map<String, String> objectMap = objectNodeList.stream()
				.filter(a -> a.path("class") !=null && !a.path("class").asText().isEmpty())
				.collect(Collectors.groupingBy(a->StringUtils.lowerCase(StringUtils.deleteWhitespace(a.path("class").asText())),
						Collectors.mapping(
								j -> Integer.valueOf((int) Double.parseDouble(j.path("timecode").asText())).toString(),
								Collectors.joining(","))));

		Map<String, String> faceMap = faceNodeList.stream()
				.filter(a -> a.path("face") !=null && !a.path("face").asText().isEmpty())
				.collect(Collectors.groupingBy(a->StringUtils.lowerCase(StringUtils.deleteWhitespace(a.path("face").asText())),
						Collectors.mapping(
								j -> Integer.valueOf((int) Double.parseDouble(j.path("timecode").asText())).toString(),
								Collectors.joining(","))));


		
		Map<String, String> mapTagToTimecode = new HashMap<String, String>();
		mapTagToTimecode.putAll(objectMap);
		mapTagToTimecode.putAll(faceMap);
		log.debug("objectNodeList:" + objectNodeList.toString());
		log.debug("faceNodeList:" + faceNodeList.toString());
		log.debug("objectMap:" + objectMap.toString());
		log.debug("faceMap:" + faceMap.toString());
		log.debug("mapTagToTimecode:" + mapTagToTimecode.toString());
		return mapTagToTimecode;
	}
}
