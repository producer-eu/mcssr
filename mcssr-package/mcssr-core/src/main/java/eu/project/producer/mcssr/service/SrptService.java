package eu.project.producer.mcssr.service;

import java.io.IOException;

import org.json.JSONObject;
import org.nuxeo.ecm.core.api.DocumentModel;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

public interface SrptService {
    /** Add some methods here. **/
	
	JsonNode getRecommendedVideo(String username, JsonNode bodyRequest) throws JsonProcessingException, UnsupportedOperationException, IOException;

	int importVideoToSrpt(DocumentModel doc);
	
	int deleteVideoFromSrpt(DocumentModel doc);
	
	JSONObject createVideoRequestToSrpt(DocumentModel doc);
	
	int sendContentToSrpt(JSONObject jsonToSend, int mode, DocumentModel doc);

}
