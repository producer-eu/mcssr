package eu.project.producer.mcssr.service;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.platform.tag.TagService;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.model.ComponentContext;
import org.nuxeo.runtime.model.ComponentInstance;
import org.nuxeo.runtime.model.DefaultComponent;

import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.mail.iap.ResponseInputStream;

public class SrptServiceImpl extends DefaultComponent implements SrptService {
	
    private final Log log = LogFactory.getLog(SrptService.class);

	public static final int IMPORT_MODE = 1;

	public static final int DELETE_MODE = 2;
	
    private String srptEndpoint;
    
	private String srptImportEndpoint;

	private String srptDeleteEndpoint;
	
	private String srptApiKey;

    private ObjectMapper mapper;

    @Override
	public JsonNode getRecommendedVideo(String username, JsonNode bodyRequest)
			throws JsonProcessingException, UnsupportedOperationException, IOException {
		log.info("Request to send to SRPT " + bodyRequest.toString());

		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = null;
		if (srptEndpoint.contains("localhost")) {
			httpPost = new HttpPost(srptEndpoint);
		} else {
			httpPost = new HttpPost(srptEndpoint + username + "/enrichments_from_set");
		}

		StringEntity entity = new StringEntity(bodyRequest.toString());
		try {
			log.info("Response body sending to SRPT:" + IOUtils.toString(entity.getContent()));
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		httpPost.setHeader("Api-Key", srptApiKey);

		CloseableHttpResponse srptResponse = httpclient.execute(httpPost);
		log.info("Response Code from SRPT:" + srptResponse.getStatusLine());
		HttpEntity responseEntity = srptResponse.getEntity();
		String responseString=null;
		try {
			responseString=IOUtils.toString(responseEntity.getContent());
			log.info("Response body from SRPT: \n" + responseString);
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		JsonNode url = mapper.readTree(responseString);
		if(url==null){
			throw new IOException("Error on SRPT Response!");
		}
		log.info("Response from SRPT " + url.toString());
		httpclient.close();
		return url;
	}
    
    public int importVideoToSrpt(DocumentModel doc) {
		JSONObject jsonToSend = new JSONObject();
		JSONObject video = createVideoRequestToSrpt(doc);
		if (video == null) { //
			return 0;
		}
		try {
			jsonToSend.accumulate("video", video);
		} catch (JSONException e) {
			log.error("Error during the creation of video Request to Import on Srpt");
		}
		
		String okMsg = "The Video content has been sent corretly to SRPT";
		log.info(okMsg);
		doc.getProperty("ann:Annotation").get("srptMessage").setValue(okMsg);
		doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
		doc.getProperty("ann:Annotation").get("srptStatus").setValue("SENT");
		
		return sendContentToSrpt(jsonToSend, IMPORT_MODE, doc);
			
			/*
			 * try {
			 * 
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", okMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "SENT"); } catch (IOException e1) {
			 * e1.printStackTrace(); }
			 */
		

	}
    
    @Override
    public JSONObject createVideoRequestToSrpt(DocumentModel doc) {
		try {
			JSONObject video = new JSONObject();

			String shared_id = doc.getId();
			String source = doc.getPropertyValue("dc:source") != null ? doc.getPropertyValue("dc:source").toString()
					: "";
			String title = doc.getPropertyValue("dc:title") != null ? doc.getPropertyValue("dc:title").toString() : "";
			String path = doc.getPathAsString();
			String description = doc.getPropertyValue("dc:description") != null
					? doc.getPropertyValue("dc:description").toString() : "";
			if (doc.getProperty("vid:info").get("duration") != null) {
				Serializable duration = doc.getProperty("vid:info").getValue("duration");
				video.put("duration", duration);
			}

			String tags = Framework.getLocalService(TagService.class)
					.getDocumentTags(doc.getCoreSession(), doc.getId(), null).stream().map(t -> t.getLabel())
					.collect(Collectors.joining(","));

			video.put("shared_id", shared_id);

			video.put("source", source);
			video.put("title", title);
			video.put("path", path);
			video.put("description", description);

			video.put("tags", tags);

			return video;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
    
    
    @Override
    public int deleteVideoFromSrpt(DocumentModel doc) {
		JSONArray videos = new JSONArray();
		JSONObject shared_id = new JSONObject();
		JSONObject root = new JSONObject();

		try {
			shared_id.accumulate("shared_id", doc.getId());

			videos.put(0, shared_id);

			root.put("videos", videos);
		} catch (JSONException e) {
			log.error("Error on creating request to delete video onSrpt!");
		}

		return sendContentToSrpt(root, DELETE_MODE, doc);
	}
    
    @Override
	public int sendContentToSrpt(JSONObject jsonToSend, int mode, DocumentModel doc) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = null;

		if (IMPORT_MODE == mode) {
			httpPost = new HttpPost(srptImportEndpoint);
		} else if (DELETE_MODE == mode) {
			httpPost = new HttpPost(srptDeleteEndpoint);
		} else {
			String errMsg = "No Srpt mode value: " + mode + " found!";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return 0;
		}

		StringEntity entity = null;

		try {
			entity = new StringEntity(jsonToSend.toString());
		} catch (UnsupportedEncodingException e) {
			String errMsg = "Error on parsing Body Request to Srpt";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return 0;
		}
		try {
			log.info("Response body sending to SRPT: \n" + IOUtils.toString(entity.getContent()));
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		httpPost.setHeader("Api-Key", srptApiKey);

		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpPost);
		} catch (IOException e) {
			String errMsg = "Error on executing request towards Srpt Tool";
			log.error(errMsg);
			log.debug(e.getMessage());
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return 0;
		}
		log.info("Response from SRPT:" + response.getStatusLine());
		HttpEntity responseEntity = response.getEntity();
		int code = response.getStatusLine().getStatusCode();
		log.info("Response code from SRPT:" + code);
		try {
			log.info("Response body from SRPT: \n" + IOUtils.toString(responseEntity.getContent()));
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			httpclient.close();
			return code;
		} catch (IOException e) {
			String errMsg = "Error on closing the http connection with Srpt Tool";
			log.error(errMsg);
			doc.getProperty("ann:Annotation").get("srptMessage").setValue(errMsg);
			doc.getProperty("ann:Annotation").get("srptLastUpdate").setValue(new Long(new Date().getTime()).toString());
			doc.getProperty("ann:Annotation").get("srptStatus").setValue("FAILURE");
			/*
			 * try { DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptMessage", errMsg);
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptLastUpdate", new Long(new
			 * Date().getTime()).toString());
			 * DocumentHelper.setProperty(doc.getCoreSession(), doc,
			 * "ann:Annotation:srptStatus", "FAILURE"); } catch (IOException e1)
			 * { // TODO Auto-generated catch block e1.printStackTrace(); }
			 */
			return 0;
		}

	}
    /**
     * Component activated notification.
     * Called when the component is activated. All component dependencies are resolved at that moment.
     * Use this method to initialize the component.
     *
     * @param context the component context.
     */
    @Override
    public void activate(ComponentContext context) {
        super.activate(context);
    }

    /**
     * Component deactivated notification.
     * Called before a component is unregistered.
     * Use this method to do cleanup if any and free any resources held by the component.
     *
     * @param context the component context.
     */
    @Override
    public void deactivate(ComponentContext context) {
        super.deactivate(context);
    }

    /**
     * Application started notification.
     * Called after the application started.
     * You can do here any initialization that requires a working application
     * (all resolved bundles and components are active at that moment)
     *
     * @param context the component context. Use it to get the current bundle context
     * @throws Exception
     */
    @Override
    public void applicationStarted(ComponentContext context) {
        // do nothing by default. You can remove this method if not used.
        srptEndpoint = Framework.getProperty("srpt.enrichments.service");
        
    	srptImportEndpoint = Framework.getProperty("srpt.import.service");

    	srptDeleteEndpoint = Framework.getProperty("srpt.delete.service");
    	
    	srptApiKey = Framework.getProperty("srpt.api.key");
    	
    	mapper=new ObjectMapper();
    }

    @Override
    public void registerContribution(Object contribution, String extensionPoint, ComponentInstance contributor) {
        // Add some logic here to handle contributions
    }

    @Override
    public void unregisterContribution(Object contribution, String extensionPoint, ComponentInstance contributor) {
        // Logic to do when unregistering any contribution
    }

}
