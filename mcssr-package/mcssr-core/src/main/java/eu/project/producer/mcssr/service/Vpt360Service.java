package eu.project.producer.mcssr.service;

import org.nuxeo.ecm.core.api.DocumentModel;

public interface Vpt360Service {
	
    /** Add some methods here. **/
	void create360VideoContent(DocumentModel doc);
	
	void startPreRenderingOf360VideoContent(DocumentModel doc);
	
}
