package eu.project.producer.mcssr.service;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.model.ComponentContext;
import org.nuxeo.runtime.model.ComponentInstance;
import org.nuxeo.runtime.model.DefaultComponent;

import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.project.producer.mcssr.exception.Vpt360Exception;

public class Vpt360ServiceImpl extends DefaultComponent implements Vpt360Service {

	private final Log log = LogFactory.getLog(SrptService.class);

	private String video360ToolEndpoint;

	private String video360VptToken;

	private ObjectMapper mapper;

	@Context
	private DocumentService documentService;

	/**
	 * Component activated notification. Called when the component is activated.
	 * All component dependencies are resolved at that moment. Use this method
	 * to initialize the component.
	 *
	 * @param context
	 *            the component context.
	 */
	@Override
	public void activate(ComponentContext context) {
		super.activate(context);
	}

	/**
	 * Component deactivated notification. Called before a component is
	 * unregistered. Use this method to do cleanup if any and free any resources
	 * held by the component.
	 *
	 * @param context
	 *            the component context.
	 */
	@Override
	public void deactivate(ComponentContext context) {
		super.deactivate(context);
	}

	/**
	 * Application started notification. Called after the application started.
	 * You can do here any initialization that requires a working application
	 * (all resolved bundles and components are active at that moment)
	 *
	 * @param context
	 *            the component context. Use it to get the current bundle
	 *            context
	 * @throws Exception
	 */
	@Override
	public void applicationStarted(ComponentContext context) {
		// do nothing by default. You can remove this method if not used.
		video360ToolEndpoint = Framework.getProperty("360video.prerendering.service");
		video360VptToken = Framework.getProperty("360video.service.accessToken");
		mapper = new ObjectMapper();
		documentService = Framework.getService(DocumentService.class);
	}

	@Override
	public void registerContribution(Object contribution, String extensionPoint, ComponentInstance contributor) {
		// Add some logic here to handle contributions
	}

	@Override
	public void unregisterContribution(Object contribution, String extensionPoint, ComponentInstance contributor) {
		// Logic to do when unregistering any contribution
	}

	@Override
	public void startPreRenderingOf360VideoContent(DocumentModel doc) {
		String video360Id = (String) doc.getPropertyValue("mul:video360Id");
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPatch httpPatch = null;
		if (video360ToolEndpoint.contains("localhost")) {
			httpPatch = new HttpPatch(video360ToolEndpoint);
		} else {
			httpPatch = new HttpPatch(video360ToolEndpoint + "/" + video360Id);
		}
		URL url=null;
		if (doc.getProperty("file:content") != null && doc.getProperty("file:content").get("digest") != null) {
			String digest = (String) doc.getProperty("file:content").get("digest").getValue();
			url = documentService.getPreSignedAmazonURL(digest);
		}
		JsonNode requestBody=null;
		try {
			Writer writer = new StringWriter();
			JsonGenerator jg = mapper.getFactory().createGenerator(writer);
			jg.writeStartObject();
			jg.writeStringField("mcssr_uid", doc.getId());
			jg.writeStringField("video_url", url.toString());
			jg.writeNumberField("anglestep_h", (long) doc.getPropertyValue("mul:video360anglestep_h"));
			jg.writeNumberField("anglestep_v", (long) doc.getPropertyValue("mul:video360anglestep_v"));
			jg.writeNumberField("bitrate", (long) doc.getPropertyValue("mul:video360bitrate"));
			jg.writeNumberField("gop_size", (long) doc.getPropertyValue("mul:video360gopSize"));
			jg.writeNumberField("init_zoom", (long) doc.getPropertyValue("mul:video360initZoom"));
			jg.writeEndObject();
			jg.close();
			requestBody = mapper.readTree(writer.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error during the creation of request to 360 VP tool!");
		}
		
		StringEntity entity = null;
		try {
			entity = new StringEntity(requestBody.toString());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error during the creation of request to 360 VP tool!");
		}
		httpPatch.setEntity(entity);
		try {
			log.info("Request body sending to 360VPT: \n" + IOUtils.toString(entity.getContent()));
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		httpPatch.setHeader("Accept", "application/json");
		httpPatch.setHeader("Content-type", "application/json");
		httpPatch.setHeader("access-token", video360VptToken);
		CloseableHttpResponse video360Response = null;
		try {
			video360Response = httpclient.execute(httpPatch);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error during communication to 360 VP tool!");
		}
		log.info("Response Code from 360VPT:" + video360Response.getStatusLine());
		JsonNode response = parseVideo360VPResponse(video360Response.getEntity());
		try {
			httpclient.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error on closing the communication to 360 VP tool!");
		}
		
	}

	@Override
	public void create360VideoContent(DocumentModel doc) throws Vpt360Exception {
		JsonNode requestBody = null;
		try {
			Writer writer = new StringWriter();
			JsonGenerator jg = mapper.getFactory().createGenerator(writer);
			jg.writeStartObject();
			jg.writeStringField("title", (String) doc.getPropertyValue("dc:title"));
			jg.writeStringField("mcssr_uid", doc.getId());
			jg.writeEndObject();
			jg.close();
			requestBody = mapper.readTree(writer.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error during the creation of request to 360 VP tool!");
		}

		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(video360ToolEndpoint);

		StringEntity entity = null;
		try {
			entity = new StringEntity(requestBody.toString());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error during the creation of request to 360 VP tool!");
		}
		try {
			log.info("Request body sending to 360VPT: \n" + IOUtils.toString(entity.getContent()));
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		httpPost.setEntity(entity);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		httpPost.setHeader("access-token", video360VptToken);

		CloseableHttpResponse video360Response = null;
		try {
			video360Response = httpclient.execute(httpPost);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error during communication to 360 VP tool!");
		}
		log.info("Response Code from 360VPT:" + video360Response.getStatusLine());
		JsonNode response = parseVideo360VPResponse(video360Response.getEntity());
		
		try {
			httpclient.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error on closing the communication to 360 VP tool!");
		}
		documentService.updateDocProperty(doc, "mul:video360title", response.path("video").path("title").asText());
		documentService.updateDocProperty(doc, "mul:video360Id", response.path("video").path("_id").asText());
	}
	
	private JsonNode parseVideo360VPResponse(HttpEntity responseEntity){
		String responseBodyString=null;
		try {
			responseBodyString=IOUtils.toString(responseEntity.getContent());
			log.info("Response body from 360VPT: \n" + responseBodyString);
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		JsonNode response = null;
		try {
			response = mapper.readTree(responseBodyString);
		} catch (UnsupportedOperationException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Vpt360Exception("Error on parsing the response from 360 VP tool!");
		}

		log.info("Response from 360 VPT " + response.toString());
		return response;
	}
}
