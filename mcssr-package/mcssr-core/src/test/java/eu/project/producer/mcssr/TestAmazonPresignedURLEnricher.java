package eu.project.producer.mcssr;

import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;

public class TestAmazonPresignedURLEnricher {
	
	private static final Log log = LogFactory.getLog(TestAmazonPresignedURLEnricher.class);

	@Test
	public void testAmazonClient() {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIBP3RYACBXF43K3Q",
				"g9RtOhwwsW+WCJ8FYPdDcaQEqdxy1Xrqd8wJ1K9U");
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCreds))
				.withRegion(Regions.fromName("eu-west-1")).build();

		Assert.assertNotNull("The Basic Credential object should not be null", awsCreds);
		Assert.assertNotNull("The Amazon S3 client  should not be null", s3Client);

		java.util.Date expiration = new java.util.Date();
		long msec = expiration.getTime();
		msec += 1000 * 60 * 60; // 1 hour.
		expiration.setTime(msec);

		GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest("producer-project-eu",
				"307be4ee23aa8fbbbec8d8efc77d93ab");
		generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
		generatePresignedUrlRequest.setExpiration(expiration);

		URL s = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
		Assert.assertNotNull("The URL should not be null", s);
		System.out.println("SignedURL: "+s);
	}

}
