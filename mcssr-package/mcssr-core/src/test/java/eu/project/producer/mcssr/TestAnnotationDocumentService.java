package eu.project.producer.mcssr;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationContext;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.util.Properties;
import org.nuxeo.ecm.automation.test.AutomationFeature;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.test.CoreFeature;
import org.nuxeo.ecm.core.test.DefaultRepositoryInit;
import org.nuxeo.ecm.core.test.annotations.Granularity;
import org.nuxeo.ecm.core.test.annotations.RepositoryConfig;
import org.nuxeo.ecm.platform.test.PlatformFeature;
import org.nuxeo.runtime.RuntimeService;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.LocalDeploy;

@RunWith(FeaturesRunner.class)
@Features({AutomationFeature.class, CoreFeature.class, PlatformFeature.class})
@RepositoryConfig(init = DefaultRepositoryInit.class, cleanup = Granularity.CLASS)
@Deploy("eu.project.producer.mcssr.mcssr-core")
public class TestAnnotationDocumentService {

    @Inject
    protected CoreSession session;
    
    @Inject
    protected AutomationService automationService;

    @Test
    public void isNuxeoStarted(){
        Assert.assertNotNull("Runtime is not available", Framework.getRuntime());
    }
    
    @Test
    public void isComponentLoaded(){
    	RuntimeService runtime = Framework.getRuntime();
        Assert.assertNotNull(runtime.getComponent("eu.project.producer.mcssr.mcssr-core"));
    }
    
    @Test
    public void shouldCallTheOperation() throws OperationException {
        OperationContext ctx = new OperationContext(session);
        DocumentModel doc = (DocumentModel) automationService.run(ctx, AnnotationDocumentService.ID);
        Assert.assertEquals("/", doc.getPathAsString());
    }

    @Test
    public void shouldCallWithParameters() throws OperationException {
        final String path = "/Producer_Repository/workspaces";
        final String tags="pluto,cicciobello";
        final Properties prop=new Properties();
        prop.put("pluto", "10,20,30");
        prop.put("cicciobello", "15,30,45");
        
        OperationContext ctx = new OperationContext(session);
        Map<String, Object> params = new HashMap<>();
        params.put("path", path);
        params.put("tags", tags);
        params.put("username", "pasquale");
        params.put("timecodes", prop);
        DocumentModel doc = (DocumentModel) automationService.run(ctx, AnnotationDocumentService.ID, params);
        Assert.assertEquals(path, doc.getPathAsString());
    }
}
