package eu.project.producer.mcssr;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.ecm.automation.test.AutomationFeature;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.PropertyException;
import org.nuxeo.ecm.core.event.EventService;
import org.nuxeo.ecm.core.event.impl.EventListenerDescriptor;
import org.nuxeo.ecm.core.test.CoreFeature;
import org.nuxeo.ecm.core.test.DefaultRepositoryInit;
import org.nuxeo.ecm.core.test.annotations.Granularity;
import org.nuxeo.ecm.core.test.annotations.RepositoryConfig;
import org.nuxeo.ecm.platform.test.PlatformFeature;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.inject.Inject;

@RunWith(FeaturesRunner.class)
@Features({AutomationFeature.class, CoreFeature.class, PlatformFeature.class})
@RepositoryConfig(init = DefaultRepositoryInit.class, cleanup = Granularity.CLASS)
@Deploy("eu.project.producer.mcssr.mcssr-core")
public class TestCreatedDocumentListener {

    protected final List<String> events = Arrays.asList("documentCreated");

    @Inject
    protected EventService s;
    
    @Inject
    protected CoreSession session;
    
    //@Test
    public void listenerFire() {
        final String path = "/default-domain/workspaces/";
        final String fileName="ZeldaMajorasMask_100p_655_HQ_part13_512kb.mp4";
        final String type="Video";
        final String description="description";
        final String repository="repository";
        
        
        DocumentModel doc=session.createDocumentModel(path,fileName,type);
        doc.setPropertyValue("dc:title", fileName);
        doc.setPropertyValue("dc:description", description);
        doc.setPropertyValue("dc:source", repository);
        doc.setPropertyValue("mul:url", "url");
        doc.setPropertyValue("mul:encoding", "encoding");
        doc.setPropertyValue("mul:mime-type", "mime-type");
        doc.setPropertyValue("name", fileName);
        doc = session.createDocument(doc);
        assertNotNull(doc);
        
    }
    
    //@Test
    public void listenerRegistration() {
        EventListenerDescriptor listener = s.getEventListener("createddocumentlistener");
        assertNotNull(listener);
        assertTrue(events.stream().allMatch(listener::acceptEvent));
    }
    
    public BiFunction<String, List<String>,List<String>> addTagToTheList=(timecode,timecodes)->{
    	timecodes.add(timecode);
    	return timecodes;
    };
    
    @Test
    public void testAnnotationJsonParsing(){
    	InputStream is=this.getClass().getResourceAsStream("annotationExample.json");
    	Assert.assertNotNull("The InputStream should be not null",is);
    	String annnotationJsonString = null;
    	try {
			annnotationJsonString=IOUtils.toString(is);
		Assert.assertNotNull("The annotation Json String should not be null",annnotationJsonString);
		ObjectMapper mapper=new ObjectMapper();
		JsonNode annJsonNode=mapper.readTree(annnotationJsonString);
		System.out.println("ANN NODE"+annJsonNode.toString());
		Assert.assertNotNull("The annotation Json Node should not be null",annJsonNode);
		ArrayNode objectNode=(ArrayNode)annJsonNode.path("ann:Annotation").path("objectdetection");
		System.out.println("OBJECT NODE"+objectNode.toString());
		Assert.assertNotNull("The objeCtDetection Json Node should not be null",objectNode);
		Assert.assertTrue("The object node is an array",objectNode.isArray());
		ArrayNode faceNode=(ArrayNode)annJsonNode.path("ann:Annotation").path("facedetection");
		System.out.println("FACE NODE"+faceNode.toString());
		Assert.assertNotNull("The Face Detection Json Node should not be null",faceNode);
		Assert.assertTrue("The face node is an array",faceNode.isArray());
		List<JsonNode> objectNodeList=IteratorUtils.toList(objectNode.iterator());
		List<JsonNode> faceNodeList=IteratorUtils.toList(faceNode.iterator());
		
		Map<String,String> objectMap=objectNodeList.stream()
				.collect(Collectors.groupingBy(a->StringUtils.lowerCase(a.path("class").asText()),
						Collectors.mapping(j->Integer.valueOf((int)Double.parseDouble(j.path("timecode").asText())).toString(), 
								Collectors.joining(","))));
		
		Map<String,String> faceMap=faceNodeList.stream()
				.collect(Collectors.groupingBy(a->StringUtils.lowerCase(a.path("face").asText()),Collectors.mapping(j->Integer.valueOf((int)Double.parseDouble(j.path("timecode").asText())).toString(), Collectors.joining(","))));
		
		Map<String,String> mapTagToTimecode=new HashMap<String,String>();
		mapTagToTimecode.putAll(objectMap);
		mapTagToTimecode.putAll(faceMap);
		
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @Test
    public void testAnnotationEmptyJsonParsing(){
    	System.out.println("testAnnotationEmptyJsonParsing");
    	InputStream is=this.getClass().getResourceAsStream("annotationExampleEmpty.json");
    	Assert.assertNotNull("The InputStream should be not null",is);
    	String annnotationJsonString = null;
    	try {
			annnotationJsonString=IOUtils.toString(is);
		Assert.assertNotNull("The annotation Json String should not be null",annnotationJsonString);
		ObjectMapper mapper=new ObjectMapper();
		JsonNode annJsonNode=mapper.readTree(annnotationJsonString);
		System.out.println("ANN NODE"+annJsonNode.toString());
		Assert.assertNotNull("The annotation Json Node should not be null",annJsonNode);
		ArrayNode objectNode=(ArrayNode)annJsonNode.path("ann:Annotation").path("objectdetection");
		System.out.println("OBJECT NODE"+objectNode.toString());
		Assert.assertNotNull("The objeCtDetection Json Node should not be null",objectNode);
		Assert.assertTrue("The object node is an array",objectNode.isArray());
		ArrayNode faceNode=(ArrayNode)annJsonNode.path("ann:Annotation").path("facedetection");
		System.out.println("FACE NODE"+faceNode.toString());
		Assert.assertNotNull("The Face Detection Json Node should not be null",faceNode);
		Assert.assertTrue("The face node is an array",faceNode.isArray());
		List<JsonNode> objectNodeList=IteratorUtils.toList(objectNode.iterator());
		List<JsonNode> faceNodeList=IteratorUtils.toList(faceNode.iterator());
		
		
		
		
		Map<String,String> objectMap=objectNodeList.stream()
				.filter(a -> a.path("class") !=null && !a.path("class").asText().isEmpty())
				.collect(Collectors.groupingBy(a->StringUtils.lowerCase(a.path("class").asText()),
						Collectors.mapping(j->Integer.valueOf((int)Double.parseDouble(j.path("timecode").asText())).toString(), 
								Collectors.joining(","))));
		
	
		Assert.assertTrue("The object Map should be empty",objectMap.isEmpty());
		
		Map<String,String> faceMap=faceNodeList.stream()
				.filter(a -> a.path("face") !=null && !a.path("face").asText().isEmpty())
				.collect(Collectors.groupingBy(a->StringUtils.lowerCase(a.path("face").asText()),Collectors.mapping(j->Integer.valueOf((int)Double.parseDouble(j.path("timecode").asText())).toString(), Collectors.joining(","))));
		
		Map<String,String> mapTagToTimecode=new HashMap<String,String>();
		mapTagToTimecode.putAll(objectMap);
		mapTagToTimecode.putAll(faceMap);
		
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	
    	
    	
		JSONObject jsonAnnotation = null;

		ObjectMapper mapper = new ObjectMapper();
		//mapper.setSerializationInclusion(Include.NON_NULL);
		//mapper.setSerializationInclusion(Include.NON_EMPTY);
		
		try {
			jsonAnnotation = new JSONObject(annnotationJsonString);
			System.out.println("jsonAnnotation:" + jsonAnnotation.toString());
		} catch (PropertyException e1) {
			// TODO Auto-generated catch block
			System.out.println("PropertyException e1"+e1.toString());
			e1.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			System.out.println("JSONException e1"+e1.toString());
			e1.printStackTrace();
		}
		JsonNode jNode = null;
		try {
			jNode = mapper.readTree(jsonAnnotation.toString());
			//jNode = mapper.readValue(jsonAnnotation.toString(), JsonNode.class);
			System.out.println("jNode:" + jNode.toString());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("ANNOTATION NODE" + jNode.toString());
    	
    	
    }
    
    

}
