package eu.project.producer.mcssr;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.ecm.core.api.Blob;
import org.nuxeo.ecm.core.api.Blobs;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.PathRef;
import org.nuxeo.ecm.core.test.CoreFeature;
import org.nuxeo.ecm.core.test.DefaultRepositoryInit;
import org.nuxeo.ecm.core.test.annotations.Granularity;
import org.nuxeo.ecm.core.test.annotations.RepositoryConfig;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

import eu.project.producer.mcssr.service.DocumentService;

@RunWith(FeaturesRunner.class)
@Features(CoreFeature.class)
@RepositoryConfig(init = DefaultRepositoryInit.class, cleanup = Granularity.CLASS)
public class TestDocumentService {
    @Inject
    CoreSession session;

    @Test
    public void theSessionIsUsable() throws Exception {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getDocument(new PathRef("/default-domain")));
        Assert.assertNotNull(createDocumentModel());
    }
    
    
    private DocumentModel createDocumentModel(){
    	DocumentModel doc=session.createDocumentModel("/default-domain/workspaces", "test", "File");
        Blob blob = Blobs.createBlob("test");
        blob.setFilename("myfile");
        blob.setDigest("mydigest");
        doc.setPropertyValue("file:content", (Serializable) blob);
        doc = session.createDocument(doc);
        doc = session.getDocument(doc.getRef());
        Assert.assertEquals("myfile", doc.getPropertyValue("file:content/name"));
        Assert.assertEquals("mydigest", doc.getPropertyValue("file:content/digest"));
        //JsonNode jsonNode=DocumentService.getJSONRepresentationOfProperty((String)doc.getPropertyValue("file:content"));
        //Assert.assertNotNull(jsonNode);
    	return doc;
    }
}
