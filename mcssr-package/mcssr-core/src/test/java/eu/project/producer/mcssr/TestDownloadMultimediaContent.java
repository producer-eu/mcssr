package eu.project.producer.mcssr;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationContext;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.test.AutomationFeature;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.test.CoreFeature;
import org.nuxeo.ecm.core.test.DefaultRepositoryInit;
import org.nuxeo.ecm.core.test.annotations.Granularity;
import org.nuxeo.ecm.core.test.annotations.RepositoryConfig;
//import org.nuxeo.ecm.platform.tag.TagService;
import org.nuxeo.ecm.platform.test.PlatformFeature;
import org.nuxeo.runtime.RuntimeService;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.RuntimeFeature;

import javax.inject.Inject;

//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

@RunWith(FeaturesRunner.class)
@Features({ AutomationFeature.class, RuntimeFeature.class, CoreFeature.class, PlatformFeature.class})
@RepositoryConfig(init = DefaultRepositoryInit.class, cleanup = Granularity.METHOD)
@Deploy("eu.project.producer.mcssr.mcssr-core")
public class TestDownloadMultimediaContent {

    @Inject
    protected CoreSession session;

    @Inject
    protected AutomationService automationService;

    /*@Inject
    protected TagService tagservice;*/

    @Test
    public void isNuxeoStarted(){
        Assert.assertNotNull("Runtime is not available", Framework.getRuntime());
    }
    
    @Test
    public void isComponentLoaded(){
    	RuntimeService runtime = Framework.getRuntime();
        Assert.assertNotNull(runtime.getComponent("eu.project.producer.mcssr.mcssr-core"));
    }

    @Test
    public void shouldCallWithParameters() {
        final String path = "/Producer_Repository/workspaces/Video";
        final String url="https://ia902605.us.archive.org/7/items/ZeldaMajorasMask_100p_655/ZeldaMajorasMask_100p_655_HQ_part13_512kb.mp4";
        final String encoding="UTF-8";
        final String fileName="ZeldaMajorasMask_100p_655_HQ_part13_512kb.mp4";
        final String mimeType="video/mp4";
        final String type="Video";
        final String tags="zelda";
        final String description="description";
        final String license="license";
        final String repository="repository";
        OperationContext ctx = new OperationContext(session);
        
        Map<String, Object> params = new HashMap<>();
        params.put("path", path);
        params.put("url", url);
        params.put("encoding", encoding);
        params.put("fileName", fileName);
        params.put("mimeType", mimeType);
        params.put("type", type);
        params.put("tags", tags);
        params.put("description", description);
        params.put("license", license);
        params.put("repository", repository);
        DocumentModel doc;
        try {
			doc = (DocumentModel) automationService.run(ctx, DownloadMultimediaContent.ID, params);
			assertNotNull(doc);
		} catch (OperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
    
    
}
