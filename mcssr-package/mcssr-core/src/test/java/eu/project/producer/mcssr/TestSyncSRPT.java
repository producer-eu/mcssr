package eu.project.producer.mcssr;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.OperationContext;
import org.nuxeo.ecm.automation.OperationException;
import org.nuxeo.ecm.automation.core.annotations.Context;
import org.nuxeo.ecm.automation.test.AutomationFeature;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.ecm.core.test.DefaultRepositoryInit;
import org.nuxeo.ecm.core.test.annotations.Granularity;
import org.nuxeo.ecm.core.test.annotations.RepositoryConfig;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.project.producer.mcssr.service.DocumentService;

public class TestSyncSRPT {

	private static final String srptGetAllEndpoint = "http://srp.producer-toolkit.eu/api/video";
	
	private static final String srptImportDEV = "http://localhost:4444/echo";

	private static final String getmcssrlist= "http://localhost:4444/mcssrlist";
   
	@Test
    public void shouldCallTheOperation() throws OperationException {
    	
		List<String> mcssrlist = getmcssrlist();
		List<String> srptlist = getsrptlist();
		List<String> listToUpdate = new ArrayList();
		List<String> listToRemove = new ArrayList();
		
		System.out.println("mcssrvideolist:"+mcssrlist);
		System.out.println("PROD srptlist:"+srptlist);
		
		//mcssrlist.removeAll(srptlist);

		listToUpdate = mcssrlist.stream()
				.filter(a->!srptlist.contains(a))
				.collect(Collectors.toList());
		
		System.out.println("final listToUpdate - items in mcssr but not in srpt:"+listToUpdate);
				
		listToRemove = srptlist.stream()
				.filter(a->!mcssrlist.contains(a))
				.collect(Collectors.toList());
		
		System.out.println("final listToRemove - items in srpt but not in mcssr:"+listToRemove);

    }
    

	private List<String> getmcssrlist() {
		System.out.println("into getmcssrlist");
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = null;
		httpGet = new HttpGet(getmcssrlist);
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpGet);
		} catch (IOException e) {
			String errMsg = "Error on executing request towards fake MCSSR list ";
			System.out.println(errMsg);
			System.out.println(e.getMessage());
		}
		
		HttpEntity entity = response.getEntity();
		String responseString = null;
		try {
			responseString = EntityUtils.toString(entity, "UTF-8");
			//System.out.println("Response responseEntity from SRPT:" + responseString);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		List<String> mcssrlist = new ArrayList();
	    try {
			JsonNode docs = mapper.readTree(responseString);
			JsonNode arrNode = docs.get("entries");
			if (arrNode.isArray()) {
			    for (final JsonNode objNode : arrNode) {
			    	if(objNode.get("uid").asText() != null) {
			    		System.out.println("mcssr uid:"+objNode.get("uid"));
				        mcssrlist.add(objNode.get("uid").asText());
			    	}
			    }
			}
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return mcssrlist;
		
	}
    
    
    private List<String> getsrptlist() {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = null;
		httpGet = new HttpGet(srptGetAllEndpoint);
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpGet);
		} catch (IOException e) {
			String errMsg = "Error on executing request towards Srpt Tool";
			System.out.println(errMsg);
			System.out.println(e.getMessage());
		}
		
		HttpEntity entity = response.getEntity();
		String responseString = null;
		try {
			responseString = EntityUtils.toString(entity, "UTF-8");
			//System.out.println("Response responseEntity from SRPT:" + responseString);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		List<String> srptlist = new ArrayList();
	    try {
			JsonNode arrNode = mapper.readTree(responseString);
			if (arrNode.isArray()) {
				int i = 0;
			    for (final JsonNode objNode : arrNode) {
			    	if(!objNode.get("euscreen").asText().contains("EUS_")) {
			    		i++;
				        System.out.println("n°"+i+" - srpt uid:"+objNode.get("euscreen"));
				        srptlist.add(objNode.get("euscreen").asText());
			    	}
			    }
			    
			}
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		int code = response.getStatusLine().getStatusCode();
		System.out.println("Response code from SRPT:" + code);
		try {
			httpclient.close();
		} catch (IOException e) {
			String errMsg = "Error on closing the http connection with Srpt Tool";
			System.out.println(errMsg);
			
		}
		
		return srptlist;
		
	}
    
    
    
    
	
  

    
}
