package eu.project.producer.mcssr;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.ecm.automation.AutomationService;
import org.nuxeo.ecm.automation.test.AutomationFeature;
import org.nuxeo.ecm.core.api.CoreSession;
import org.nuxeo.ecm.core.test.CoreFeature;
import org.nuxeo.ecm.core.test.DefaultRepositoryInit;
import org.nuxeo.ecm.core.test.annotations.Granularity;
import org.nuxeo.ecm.core.test.annotations.RepositoryConfig;
import org.nuxeo.ecm.platform.test.PlatformFeature;
import org.nuxeo.runtime.RuntimeService;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.RuntimeFeature;

@RunWith(FeaturesRunner.class)
@Features({ AutomationFeature.class, RuntimeFeature.class, CoreFeature.class, PlatformFeature.class})
@RepositoryConfig(init = DefaultRepositoryInit.class, cleanup = Granularity.METHOD)
@Deploy({"eu.project.producer.mcssr.mcssr-core"})
public class TestUpdateEnrichedContent {

	  @Inject
	    protected CoreSession session;

	    @Inject
	    protected AutomationService automationService;

	    /*@Inject
	    protected TagService tagservice;*/

	    @Test
	    public void isNuxeoStarted(){
	        Assert.assertNotNull("Runtime is not available", Framework.getRuntime());
	    }
	    
	    @Test
	    public void isComponentLoaded(){
	    	RuntimeService runtime = Framework.getRuntime();
	        Assert.assertNotNull(runtime.getComponent("eu.project.producer.mcssr.mcssr-core"));
	    }

}
