package eu.project.producer.mcssr;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.nuxeo.ecm.automation.OperationException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestVideoRecommender {

	// @Test
	public void shouldCallWithParameters() throws OperationException, ClientProtocolException, IOException {

		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://postman-echo.com/post");
		httpPost.addHeader("Content-Type", "application/json");
		StringEntity params = new StringEntity("{ \"marker\":\"test\" }");
		httpPost.setEntity(params);
		CloseableHttpResponse response = httpclient.execute(httpPost);
		int code = response.getStatusLine().getStatusCode();

		try {

			HttpEntity entity2 = response.getEntity();
			String responsestring = new BasicResponseHandler().handleResponse(response);
			JSONObject resp = new JSONObject();
			System.out.println("response:" + responsestring);
			// do something useful with the response body
			// and ensure it is fully consumed
			EntityUtils.consume(entity2);
		} finally {
			response.close();
		}

	}

	@Test
	public void testJsonItemsParsing() {
		InputStream is = this.getClass().getResourceAsStream("itemsArray.json");
		Assert.assertNotNull("The InputStream should be not null", is);
		String itemsArrayJsonString = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			itemsArrayJsonString = IOUtils.toString(is);
			Assert.assertNotNull("The itemsArray Json String should not be null", itemsArrayJsonString);
			// JsonNode
			// itemsJsonNode=DocumentService.getJSONRepresentationOfProperty(itemsArrayJsonString);
			// System.out.println("ITEM NODE: "+itemsJsonNode.toString());
			// JsonNode
			// unescapedItemsJsonNode=DocumentService.convertItemsArrayWithUnescapedMarkers(itemsJsonNode);
			// System.out.println("UNESCAPED ITEM NODE:
			// "+itemsJsonNode.toString());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testJsonGenerator() {
		com.fasterxml.jackson.core.JsonFactory factory = new com.fasterxml.jackson.core.JsonFactory();
		Writer stringWriter = new StringWriter();
		JsonGenerator jg = null;
		try {
			jg = factory.createGenerator(stringWriter);
			jg.writeStartObject();
			jg.writeStringField("type", "Video");

			jg.writeObjectFieldStart("ann:Annotation");
			jg.writeStringField("aatLastUpdate", null);
			jg.writeObjectFieldStart("transcription");
			jg.writeStringField("url", null);
			jg.writeEndObject();
			jg.writeStringField("srptStatus", "SENT");

			jg.writeArrayFieldStart("objectdetection");

			jg.writeStartObject();
			jg.writeStringField("frameIndex", null);
			jg.writeObjectFieldStart("position");
			jg.writeStringField("xaxis", "458.05640");
			jg.writeStringField("yaxis", "32.05d640");
			jg.writeEndObject();
			jg.writeNumberField("probability", 0.78215);
			jg.writeEndObject();

			jg.writeStartObject();
			jg.writeStringField("frameIndex", null);
			jg.writeObjectFieldStart("position");
			jg.writeStringField("xaxis", "458.05640");
			jg.writeStringField("yaxis", "32.05d640");
			jg.writeEndObject();
			jg.writeNumberField("probability", 0.78215);
			jg.writeEndObject();
			jg.writeEndArray();

			jg.writeArrayFieldStart("markers");
			jg.writeString("sds");
			jg.writeString("sds");
			jg.writeEndArray();

			jg.writeEndObject();
			jg.close();
			System.out.println(stringWriter.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
