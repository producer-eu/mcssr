package eu.project.producer.mcssr.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.ecm.platform.test.PlatformFeature;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;

import com.google.inject.Inject;

@RunWith(FeaturesRunner.class)
@Features({ PlatformFeature.class })
@Deploy("eu.project.producer.mcssr.mcssr-core")
public class TestVpt360Service {

    @Inject
    protected Vpt360Service vpt360service;

    @Test
    public void testService() {
        assertNotNull(vpt360service);
    }
}
