/*
 * Author: Giuseppe Filomena
 */

package org.nuxeo.ecm.platform.oauth2.openid.auth;

import java.util.Date;

import org.json.JSONException;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.Key;

public class Auth0UserInfo extends GenericJson implements OpenIDUserInfo {
	
    @Key("firstName")
    protected String firstName;

    @Key("lastName")
    protected String lastName;
    
    @Key("nickname")
    protected String nickname;
    
    @Key("picture")
    protected String picture;
    
    @Key("email")
    protected String email;

    @Key("email_verified")
    protected boolean emailVerified;
    
    @Key("https://producer.eu/user_metadata")
    protected User_Metadata user_metadata;
    
    @Key("jobTitle")
    protected String jobTitle;
    
    @Key("userType")
    protected String userType;
    
    @Key("nationality")
    protected String nationality;
    
    @Key("town")
    protected String town;

	@Key("country")
    protected String country;
    
    @Key("countryId")
    protected String countryId;
    
    @Key("education")
    protected String education;
    
    @Key("educationId")
    protected String educationId;
    
    @Key("ageGroup")
    protected String ageGroup;
    
    @Key("ageGroupId")
    protected String ageGroupId;
    
    @Key("occupation")
    protected String occupation;
    
    @Key("occupationId")
    protected String occupationId;
    
    @Key("gender")
    protected String gender;
    
    @Key("genderId")
    protected String genderId;

    @Key("updated_at")
    protected String updated_at;

    public String getFirstName() {
    	String firstName;
    	try {
    		firstName = getUser_Metadata().getUser().getString("firstName");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return firstName;
    }


	public String getLastName() {
    	String lastName;
    	try {
    		lastName = getUser_Metadata().getUser().getString("lastName");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return lastName;
	}


    @Override
    public String getNickname() {
        return nickname;
    }
    
    public User_Metadata getUser_Metadata() {
		return user_metadata;
	}

    @Override
    public String getPicture() {
        return picture;
    }


    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public boolean isEmailVerified() {
        return emailVerified;
    }

    @Override
    public String getPhoneNumber() {
    	String phoneNumber;
    	try {
    		phoneNumber = getUser_Metadata().getUser().getString("phoneNumber");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return phoneNumber;
    }

    public Date getUpdatedAt() {
        Date date;
        try {
            DateTime dateTime = DateTime.parseRfc3339(updated_at);
            date = new Date(dateTime.getValue());
        } catch (NumberFormatException e) {
            return null;
        }
        return date;
    }
	


	public String getJobTitle() {
		String jobTitle;
    	try {
    		jobTitle = getUser_Metadata().getUser().getString("jobTitle");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return jobTitle;
	}

	public String getUserType() {
		String userType;
    	try {
    		userType = getUser_Metadata().getUser().getString("userType");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return userType;
	}

	public String getNationality() {
		String nationality;
    	try {
    		nationality = getUser_Metadata().getUser().getString("nationality");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return nationality;
	}

	public String getTown() {
		String town;
    	try {
    		town = getUser_Metadata().getUser().getString("town");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return town;
	}

	public String getCountry() {
		String country;
    	try {
    		country = getUser_Metadata().getUser().getString("country");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return country;
	}

	public String getCountryId() {
		String countryId;
    	try {
    		countryId = getUser_Metadata().getUser().getString("countryId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return countryId;
	}

	public String getEducation() {
		String education;
    	try {
    		education = getUser_Metadata().getUser().getString("education");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return education;
	}

	public String getEducationId() {
		String educationId;
    	try {
    		educationId = getUser_Metadata().getUser().getString("educationId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return educationId;
	}

	public String getAgeGroup() {
		String ageGroup;
    	try {
    		ageGroup = getUser_Metadata().getUser().getString("ageGroup");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return ageGroup;
	}

	public String getAgeGroupId() {
		String ageGroupId;
    	try {
    		ageGroupId = getUser_Metadata().getUser().getString("ageGroupId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return ageGroupId;
	}

	public String getOccupation() {
		String occupation;
    	try {
    		occupation = getUser_Metadata().getUser().getString("occupation");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return occupation;
	}

	public String getOccupationId() {
		String occupationId;
    	try {
    		occupationId = getUser_Metadata().getUser().getString("occupationId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return occupationId;
	}

    public String getGender() {
		String gender;
    	try {
    		gender = getUser_Metadata().getUser().getString("gender");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	return gender;
	}
	
	public String getGenderId() {
		String genderId;
    	try {
    		genderId = getUser_Metadata().getUser().getString("genderId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return genderId;
	}


	@Override
	public String getSubject() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getGivenName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getFamilyName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getMiddleName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getPreferredUsername() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getProfile() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getWebsite() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Date getBirthdate() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getZoneInfo() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getLocale() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getAddress() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Date getUpdatedTime() {
		// TODO Auto-generated method stub
		return null;
	}
}
