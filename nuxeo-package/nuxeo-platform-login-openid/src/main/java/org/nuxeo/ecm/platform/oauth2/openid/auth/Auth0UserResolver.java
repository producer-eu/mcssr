/*
 * (C) Copyright 2006-2013 Nuxeo SA (http://nuxeo.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Nelson Silva <nelson.silva@inevo.pt> - initial API and implementation
 *     Nuxeo
 */

/*
 * Author: Giuseppe Filomena
 */

package org.nuxeo.ecm.platform.oauth2.openid.auth;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuxeo.ecm.core.api.DocumentModel;
import org.nuxeo.ecm.core.api.DocumentModelList;
import org.nuxeo.ecm.core.api.NuxeoException;
import org.nuxeo.ecm.directory.DirectoryException;
import org.nuxeo.ecm.platform.oauth2.openid.OpenIDConnectProvider;
import org.nuxeo.ecm.platform.usermanager.UserManager;
import org.nuxeo.runtime.api.Framework;

/**
 * Helper class to manage mapping between identification info comming from the OpenID provider and Nuxeo UserManager.
 *
 * @author Giuseppe Filomena
 * 
 */
public class Auth0UserResolver extends UserResolver {

    private static final Log log = LogFactory.getLog(Auth0UserResolver.class);

    public Auth0UserResolver(OpenIDConnectProvider provider) {
        super(provider);
    }

    @Override
    public String findNuxeoUser(OpenIDUserInfo userInfo) {
    	DocumentModel updateduser;
        try {
            UserManager userManager = Framework.getLocalService(UserManager.class);
            Map<String, Serializable> query = new HashMap<String, Serializable>();
            query.put(userManager.getUserEmailField(), userInfo.getEmail());

            DocumentModelList users = userManager.searchUsers(query, null);

            if (users.isEmpty()) {
                return null;
            }

            DocumentModel user = users.get(0);
           // log.info("Auth0UserResolver findNuxeoUser DocumentModel user:"+user);
            updateduser = updateUserInfo(user, userInfo);
           // log.info("UPDATE - Auth0UserResolver findNuxeoUser DocumentModel updateduser:"+updateduser);
            return (String) updateduser.getPropertyValue(userManager.getUserIdField());

        } catch (NuxeoException e) {
            log.error("Error while search user in UserManager using email " + userInfo.getEmail(), e);
            return null;
        }
    }

    @Override
    public DocumentModel updateUserInfo(DocumentModel user, OpenIDUserInfo userInfo) {
    	DocumentModel updateduser;
        try {
        	
            UserManager userManager = Framework.getLocalService(UserManager.class);
            user.setPropertyValue(userManager.getUserEmailField(), userInfo.getEmail());
            
            updateduser = updateAuth0UserInfo(user, userInfo);
			
			//log.info("property schema: user: " + updateduser.getProperties("user"));
            userManager.updateUser(updateduser);
        } catch (NuxeoException e) {
            log.error("Error while search user in UserManager using email " + userInfo.getEmail(), e);
            return null;
        }
        return updateduser;
    }
    
    public DocumentModel updateAuth0UserInfo(DocumentModel user, OpenIDUserInfo userInfo) {
    	
        Auth0UserInfo info = (Auth0UserInfo) userInfo;
		user.setProperty("user", "lastName", info.getLastName());
		user.setProperty("user", "firstName", info.getFirstName());
		user.setProperty("user", "phoneNumber", info.getPhoneNumber());
		user.setProperty("user", "jobTitle", info.getJobTitle());
		user.setProperty("user", "userType", info.getUserType());
		user.setProperty("user", "nationality", info.getNationality());
		user.setProperty("user", "town", info.getTown());
		user.setProperty("user", "country", info.getCountry());
		user.setProperty("user", "countryId", info.getCountryId());
		user.setProperty("user", "education", info.getEducation());
		user.setProperty("user", "educationId", info.getEducationId());
		user.setProperty("user", "ageGroup", info.getAgeGroup());
		user.setProperty("user", "ageGroupId", info.getAgeGroupId());
		user.setProperty("user", "occupation", info.getOccupation());
		user.setProperty("user", "occupationId", info.getOccupationId());
		user.setProperty("user", "gender", info.getGender());
		user.setProperty("user", "genderId", info.getGenderId());
		
		return user;
    	
    }
    
    

}
